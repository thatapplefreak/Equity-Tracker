#!/usr/bin/env python

# This small tool requires that you have LaTeX installed (xelatex specifically) for compilation after running this and it has the supporting packages for listings.
#
# This most likely requires a Unix-based system, and if all else fails, this is known to work on ArchLinux
# with the following dependencies:
#
# python
# texlive-most



import sys
import os
import subprocess


output = str(subprocess.Popen(["find ../ -name \*java"], stdout=subprocess.PIPE, shell=True).communicate()[0])
output = output[2:-3]
files = output.split("\\n")
# print(files)
sources = ""
previous_section_name = ""
previous_subsection_name = ""

for e in files:
    directories = e.split("/")

    if previous_section_name != directories[2]:
        sources += r"\section{"+ directories[2]+"}"
        previous_section_name = directories[2]

    if len(directories) > 4 and previous_subsection_name != directories[3] :
        sources += r"\subsection{" + directories[3] +"}"
        previous_subsection_name = directories[3]


    short_element = e[7:]
    sources += "\\lstinputlisting[caption="+ short_element +  ", escapechar=, style=customJ]{"+ e +"}\n"

# print(output)
filename = sys.argv[1]


preamble = r"""
\documentclass[a4paper]{article}
\usepackage{listings} % for importing code

%math formatting with $...$
\usepackage{amsmath}
%math font, $\mathbb{...}$ will be big, double-lined letters for sets
\usepackage{amssymb}

\usepackage{subfiles}
\usepackage{pgfplots}

\usepackage{accents}

\usepackage[bookmarks]{hyperref}
\setlength\parindent{0pt} % sets indent to zero
\usepackage{xparse} % allows optional arguments on self defined commands

\lstdefinestyle{customJ}{%
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  frame=single,
  xleftmargin=\parindent,
  language=Java,
  showstringspaces=false,
  numbers=left,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{green!40!black},
  commentstyle=\itshape\color{purple!40!black},
  identifierstyle=\color{blue},
  stringstyle=\color{orange},
}

\lstdefinestyle{customBLACK}{%
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  frame=single,
  xleftmargin=\parindent,
  showstringspaces=false,
  numbers=left,
  basicstyle=\footnotesize\ttfamily,
}



\newcommand{\horiz}{\begin{center}\line(1,0){400}\end{center} } % draws a horizontal line.
\newcommand{\smallhoriz}{\\begin{center}\line(1,0){400}\end{center} } % draws a horizontal line.

% \DeclareDocumentCommand{\importcode}{m}{o}
\newcommand{\includecode}[2][J]{\lstinputlisting[caption=#2, escapechar=, style=custom#1]{#2}}


%format margins
%1.5in is about default, .5in is giant, 1.0 is slightly too wide, 1.2 looks like word
\usepackage    [margin=0.8in]{geometry}"""

file_name = "fpts.tex"


begin = "\\begin{document}"
end = "\\end{document}"

TEX_document = preamble+"\n" + begin +"\n"+ sources + end
# print(TEX_document)



with open(filename, 'w') as file_:
    file_.write(TEX_document)
