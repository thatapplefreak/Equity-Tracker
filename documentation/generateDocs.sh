#!/bin/bash
tree  ../ > sourceTree.txt
echo "" >> sourceTree.txt
echo "--- LOC of Java---" >> sourceTree.txt
find ../ -name \*.java | xargs wc -l >> sourceTree.txt
./createLatexDoc.py fpts_doc.tex
xelatex ./fpts_doc.tex

# ls -R ../src | grep > fileStructure.txt
find ../src/ -name \*.java > fileStructure.txt

# xelatex ./fpts.tex
git log --no-decorate > vclog.txt
# javac ../src/Master.java > buildlog.txt
# The buildlog was generated from within Idea

