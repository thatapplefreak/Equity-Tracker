package gui;

import cmd.DeleteUserAccountCommand;
import cmd.LogoutCommand;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ziggypop on 3/25/16.
 */
public class NavigationPanel extends JPanel implements ActionListener{

    private JButton LogoutBtn, ViewCashAccountsBtn, ViewEquitiesBtn, ViewMarketBtn, MarketSimulationBtn, TransactionHistoryBtn, WatchListBtn, deleteUserBtn;

    private Window win;

    public NavigationPanel(Window win){
        this.win = win;

        this.add(LogoutBtn = new JButton("Logout"));
        LogoutBtn.addActionListener(this);
        this.add(ViewCashAccountsBtn = new JButton("View Cash Accounts"));
        ViewCashAccountsBtn.addActionListener(this);
        this.add(ViewEquitiesBtn = new JButton("View Holdings"));
        ViewEquitiesBtn.addActionListener(this);
        this.add(ViewMarketBtn = new JButton("View Current Market"));
        ViewMarketBtn.addActionListener(this);
        this.add(MarketSimulationBtn = new JButton("Simulate the Market"));
        MarketSimulationBtn.addActionListener(this);
        this.add(TransactionHistoryBtn = new JButton("Transaction History"));
        TransactionHistoryBtn.addActionListener(this);
        this.add(WatchListBtn = new JButton("WatchList"));
        WatchListBtn.addActionListener(this);
        this.add(deleteUserBtn = new JButton("Delete User Portfolio"));
        deleteUserBtn.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object sauce = actionEvent.getSource();
        if (sauce.equals(LogoutBtn)) {
            win.commandHandler.logout(new LogoutCommand(win.commandHandler));
        } else if (sauce.equals(ViewCashAccountsBtn)) {
            win.setCurrentView(new CashAccountsView(win, win.commandHandler.getPortfolioManager().getCurrentUserPortfolio()));
        } else if (sauce.equals(ViewEquitiesBtn)) {
            win.setCurrentView(new HoldingsView(win));
        } else if (sauce.equals(MarketSimulationBtn)) {
            win.setCurrentView(new MarketSimulationView(win));
        } else if (sauce.equals(ViewMarketBtn)) {
            win.setCurrentView(new MarketView(win));
        } else if (sauce.equals(TransactionHistoryBtn)) {
            win.setCurrentView(new TransactionHistoryView(win));
        } else if(sauce.equals(WatchListBtn)){
            win.setCurrentView(new WatchListView(win));
        } else if (sauce.equals(deleteUserBtn)){
            win.commandHandler.deleteUserAccount(new DeleteUserAccountCommand(win.commandHandler));
        }
    }
}
