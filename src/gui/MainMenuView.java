package gui;

import cmd.DeleteUserAccountCommand;
import cmd.LogoutCommand;
import model.User.UserPortfolio;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created by byronzaharako on 3/10/16.
 */
public class MainMenuView extends View {

    private Window win;

    public MainMenuView(Window mainWindow) {
        this.win = mainWindow;
        NavigationPanel nav = new NavigationPanel(win);
        add(nav, BorderLayout.PAGE_START);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        //handled by the navpanel
    }

    @Override
    public void update() {
        if (win.commandHandler.getPortfolioManager().getCurrentUserPortfolio() == null) {
            //User logged out
            win.setCurrentView(new LoginView(win));
        }
    }
}
