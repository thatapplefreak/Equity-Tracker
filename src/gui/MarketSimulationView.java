package gui;

import cmd.QueueSimulationCommand;
import cmd.RunSimulationCommand;
import model.Simulations.Simulation;
import model.Simulations.Simulator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.LinkedList;

/**
 * Created by byronzaharako on 3/10/16.
 */
public class MarketSimulationView extends View {


    private final static String BACK_TEXT = "Bark";
    private final static String SIMULATIONS_TEXT = "Simulations";
    private final static String SIMULATION_TYPE_TEXT = "Simulation Type:";
    private final static String PER_ANUM_TEXT = "PerAnumChange:";
    private final static String DURATION_TEXT = "Duration (years):";

    private final static String ADD_DAY = "Add Day";
    private final static String ADD_WEEK = "Add Week";
    private final static String ADD_MONTH = "Add Month";
    private final static String ADD_YEAR = "Add Year";

    private final static String ADD_SIM = "Add Simulation to Queue";
    private final static String RUN_ONE = "Run One Simulation";
    private static final String RUN_ALL = "Run all Simulations";

    private static final String BULL = "Bull";
    private static final String BEAR = "Bear";
    private static final String NOGROWTH= "No Growth";

    LinkedList<Simulation> simulations;

    private Window win;
    //private JButton backBtn;

    private JButton runBtn;
    private JButton runAllBtn;
    private JTextField timeFeild;
    private JTextField perAnumChangeFeild;
    private JComboBox<Object> simulationSelector;

    private JScrollPane listScroller;

    private JButton queueSimulation;

    private JButton dayButton;
    private JButton weekButton;
    private JButton monthButton;
    private JButton yearButton;

    private JPanel listPane;

    private List list;

    public MarketSimulationView(Window win) {
        this.win = win;

        //JPanel boxLayoutVert = new JPanel();
        //boxLayoutVert.setLayout(new BoxLayout(boxLayoutVert, BoxLayout.Y_AXIS));

        JPanel panel = new JPanel(new BorderLayout());


        JPanel inputBoxLayout = new JPanel();
        inputBoxLayout.setLayout(new BoxLayout(inputBoxLayout, BoxLayout.Y_AXIS));
        inputBoxLayout.setBorder(BorderFactory.createLineBorder(new Color(0,0,0)));

        //JPanel backRow = new JPanel();
        //backRow.setLayout(new FlowLayout());

        JPanel inputRow1 = new JPanel();
        inputRow1.setLayout(new FlowLayout());

        JPanel inputRow2 = new JPanel();
        inputRow2.setLayout(new FlowLayout());

        JPanel inputRow3 = new JPanel();
        inputRow3.setLayout(new FlowLayout(FlowLayout.RIGHT));

        GridLayout alterDurationLayout = new GridLayout(2,2);
        JPanel alterDurationGrid = new JPanel();
        alterDurationGrid.setLayout(alterDurationLayout);

        inputRow2.add(alterDurationGrid);

        JPanel runRow = new JPanel();
        runRow.setLayout(new FlowLayout());


        NavigationPanel nav = new NavigationPanel(win);
        panel.add(nav, BorderLayout.PAGE_START);

        listPane = new JPanel();
        listPane.setLayout(new BoxLayout(listPane, BoxLayout.PAGE_AXIS));
        JLabel label = new JLabel(SIMULATIONS_TEXT);
        listPane.add(label);

        setSimulationsBox();
        listPane.add(Box.createRigidArea(new Dimension(0,5)));
        listPane.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

        String[] options = {
                BULL,
                BEAR,
                NOGROWTH
        };
        inputRow1.add(new JLabel(SIMULATION_TYPE_TEXT));
        inputRow1.add(simulationSelector = new JComboBox<Object>(options));

        inputRow1.add(new JLabel(PER_ANUM_TEXT));
        inputRow1.add(perAnumChangeFeild = new JTextField());
        perAnumChangeFeild.setText("0.0");
        perAnumChangeFeild.setColumns(8);

        inputRow2.add(new JLabel(DURATION_TEXT));
        inputRow2.add(timeFeild = new JTextField(0));
        timeFeild.setText(0+"");
        timeFeild.setColumns(16);

        alterDurationGrid.add(dayButton = new JButton(ADD_DAY));
        dayButton.addActionListener(this);
        alterDurationGrid.add(weekButton = new JButton(ADD_WEEK));
        weekButton.addActionListener(this);
        alterDurationGrid.add(monthButton = new JButton(ADD_MONTH));
        monthButton.addActionListener(this);
        alterDurationGrid.add(yearButton = new JButton(ADD_YEAR));
        yearButton.addActionListener(this);

        inputRow3.add(queueSimulation = new JButton(ADD_SIM));
        queueSimulation.addActionListener(this);

        runRow.add(runBtn = new JButton(RUN_ONE));
        runBtn.addActionListener(this);
        runRow.add(runAllBtn = new JButton(RUN_ALL));
        runAllBtn.addActionListener(this);


        //boxLayoutVert.add(backRow);

        inputBoxLayout.add(inputRow1);
        inputBoxLayout.add(inputRow2);
        inputBoxLayout.add(inputRow3);
        panel.add(inputBoxLayout, BorderLayout.LINE_START);
        panel.add(listPane, BorderLayout.CENTER);
        panel.add(runRow, BorderLayout.PAGE_END);

        this.add(panel);


    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();


        if (source.equals(dayButton)){
            timeFeild.setText(
                    (Double.parseDouble(timeFeild.getText()) + Simulator.DAY) + ""
            );
        } else if (source.equals(weekButton)){
            timeFeild.setText(
                    (Double.parseDouble(timeFeild.getText()) + Simulator.WEEK) + ""
            );
        } else if (source.equals(monthButton)) {
            timeFeild.setText(
                    (Double.parseDouble(timeFeild.getText()) + Simulator.MONTH) + ""
            );
        } else if (source.equals(yearButton)){
            timeFeild.setText(
                    (Double.parseDouble(timeFeild.getText()) + Simulator.YEAR) + ""
            );
        } else if (source.equals(runBtn)) {
            win.commandHandler.runSimulation(new RunSimulationCommand(
                    win.commandHandler,
                    true));
            update();
        } else if (source.equals(runAllBtn)) {
            win.commandHandler.runSimulation(new RunSimulationCommand(
                    win.commandHandler,
                    false));
            update();
        } else if (source.equals(queueSimulation)){
            double duration = Double.parseDouble(timeFeild.getText());
            double perAnum = Double.parseDouble(perAnumChangeFeild.getText());

            if (simulationSelector.getSelectedIndex() == 0){
                win.commandHandler.queueSimulation(new QueueSimulationCommand(
                        win.commandHandler,
                        Simulation.SimulationEnum.Bull,
                        duration,
                        perAnum));
            } else if (simulationSelector.getSelectedIndex() == 1){
                win.commandHandler.queueSimulation(new QueueSimulationCommand(
                        win.commandHandler,
                        Simulation.SimulationEnum.Bear,
                        duration,
                        perAnum));
            } else if (simulationSelector.getSelectedIndex() == 2){
                win.commandHandler.queueSimulation(new QueueSimulationCommand(
                        win.commandHandler,
                        Simulation.SimulationEnum.NoGrowth,
                        duration,
                        perAnum));
            }
            System.out.println(win.commandHandler.getSimulator().getSizeOfSimulation() + " Simulations queued");
            update();
        }


    }

    @Override
    public void update() {
        setSimulationsBox();
    }

    /**
     * somehow figure out how to update the scroll view.
     */
    private void setSimulationsBox(){
        listPane.removeAll();
        list = new List();
        simulations = win.commandHandler.getSimulator().getSimulations();
        for (Simulation sim : simulations){
            list.add(sim.toString());
        }
        listScroller = new JScrollPane(list);
        listScroller.setPreferredSize(new Dimension(350, 250));
        listScroller.setAlignmentX(CENTER_ALIGNMENT);

        listPane.add(listScroller);

        listPane.revalidate();
        listPane.repaint();

    }
}
