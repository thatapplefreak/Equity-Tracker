package gui.TableModels;

import model.MarketStructure.Market;
import model.MarketStructure.TradableEntity;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by ziggypop on 4/7/16.
 * Table model for Tradable Entities
 */
public class TradableEntityTableModel implements TableModel{



        public ArrayList<TradableEntity> tradableEntities;
        private static final int COLUMNS = 3;


        private static final String TE = "Tradable Entity";
        private static final String TS = "Ticker Symbol";
        private static final String PRICE = "Price";

        public TradableEntityTableModel(ArrayList<TradableEntity> entities){
            this.tradableEntities = entities;
        }

        @Override
        public int getRowCount() {
            return tradableEntities.size();
        }

        @Override
        public int getColumnCount() {
            return COLUMNS;
        }

        @Override
        public String getColumnName(int i) {
            switch (i){
                case 0:
                    return TE;
                case 1:
                    return TS;
                case 2:
                    return PRICE;
                default:
                    return "";
            }
        }

        @Override
        public Class<?> getColumnClass(int i) {
            Object o = getValueAt(0, i);
            if(o==null) return Object.class;
            return o.getClass();
        }

        @Override
        public boolean isCellEditable(int i, int i1) {
            return i1 == 3 || i1 == 4;
        }

        @Override
        public Object getValueAt(int i, int i1) {
            TradableEntity item = tradableEntities.get(i);
            switch(i1){
                case 0:
                    return item.getName();
                case 1:
                    return item.getTickerSymbol();
                case 2:
                    return item.getValue();
                default:
                    return "";
            }

        }

        @Override
        public void setValueAt(Object newValue, int row, int col) {
        }

        @Override
        public void addTableModelListener(TableModelListener tableModelListener) {
        }

        @Override
        public void removeTableModelListener(TableModelListener tableModelListener) {

        }


}
