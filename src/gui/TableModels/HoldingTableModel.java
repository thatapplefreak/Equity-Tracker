package gui.TableModels;

import model.MarketStructure.TradableEntity;
import model.User.Holding;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by ziggypop on 4/7/16.
 */
public class HoldingTableModel implements TableModel {

    public ArrayList<Holding> holdings;
    private static final int COLUMNS = 5;


    private static final String TE = "Tradable Entity";
    private static final String TS = "Ticker Symbol";
    private static final String PRICE = "Price";
    private static final String SHARES = "Shares Owned";
    private static final String VALUE = "Total Value";

    public HoldingTableModel(ArrayList<Holding> holdings){
        this.holdings = holdings;
    }

    @Override
    public int getRowCount() {
        return holdings.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMNS;
    }

    @Override
    public String getColumnName(int i) {
        switch (i){
            case 0:
                return TE;
            case 1:
                return TS;
            case 2:
                return PRICE;
            case 3:
                return SHARES;
            case 4:
                return VALUE;
            default:
                return "";
        }
    }

    @Override
    public Class<?> getColumnClass(int i) {
        Object o = getValueAt(0, i);
        if(o==null) return Object.class;
        return o.getClass();
    }

    @Override
    public boolean isCellEditable(int i, int i1) {
        return i1 == 3 || i1 == 4;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Holding item = holdings.get(i);
        switch(i1){
            case 0:
                return item.getTradableEntity().getName();
            case 1:
                return item.getTradableEntity().getName();
            case 2:
                return item.getTradableEntity().getValue();
            case 3:
                return item.getNumberSharesOwned();
            case 4:
                return item.getValue();
            default:
                return "";
        }

    }

    @Override
    public void setValueAt(Object newValue, int row, int col) {
    }

    @Override
    public void addTableModelListener(TableModelListener tableModelListener) {
        //I DONT KNOW WHAT TO DO HERE, probably update the model with new data if changed.
    }

    @Override
    public void removeTableModelListener(TableModelListener tableModelListener) {

    }


}
