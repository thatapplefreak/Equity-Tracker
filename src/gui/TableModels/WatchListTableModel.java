package gui.TableModels;

import exceptions.WatchListException;
import model.User.Watch.WatchItem;
import model.User.Watch.WatchList;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by ziggypop on 4/5/16.
 * A Table Model to make creating a table easier in the WatchListView.
 */
public class WatchListTableModel extends AbstractTableModel {

    public ArrayList<WatchItem> watchItems = new ArrayList<>();
    private static final int COLUMNS = 5;


    private static final String TE = "Tradable Entity";
    private static final String TS = "Ticker Symbol";
    private static final String PRICE = "Price";
    private static final String LOW_PRICE = "Low Price";
    private static final String HIGH_PRICE = "High Price";

    public WatchListTableModel(WatchList watchList){
        watchItems = watchList.getWatchList();
    }

    @Override
    public int getRowCount() {
        return watchItems.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMNS;
    }

    @Override
    public String getColumnName(int i) {
        switch (i){
            case 0:
                return TE;
            case 1:
                return TS;
            case 2:
                return PRICE;
            case 3:
                return LOW_PRICE;
            case 4:
                return HIGH_PRICE;
            default:
                return "";
        }
    }

    @Override
    public Class<?> getColumnClass(int i) {
        Object o = getValueAt(0, i);
        if(o==null) return Object.class;
        return o.getClass();
    }

    @Override
    public boolean isCellEditable(int i, int i1) {
        return i1 == 3 || i1 == 4;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        WatchItem item = watchItems.get(i);
        switch(i1){
            case 0:
                return item.getEquityName();
            case 1:
                return item.getTradableEntityTickerSymbol();
            case 2:
                return item.getTradableEntityPrice();
            case 3:
                return item.getLowTrip().getTripPrice();
            case 4:
                return item.getHighTrip().getTripPrice();
            default:
                return "";
        }

    }

    @Override
    public void setValueAt(Object newValue, int row, int col) {
        double newDouble = (Double) newValue;
        try {
            if (col == 4) {
                watchItems.get(row).setHighTrip(newDouble);
            } if (col == 3){
                watchItems.get(row).setLowTrip(newDouble);
            }
        } catch (WatchListException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds a listener to the list that is notified each time a change
     * to the data model occurs.
     *
     * @param l the TableModelListener
     */
    @Override
    public void addTableModelListener(TableModelListener l) {

    }

    /**
     * Removes a listener from the list that is notified each time a
     * change to the data model occurs.
     *
     * @param l the TableModelListener
     */
    @Override
    public void removeTableModelListener(TableModelListener l) {

    }
}
