package gui;


import cmd.*;
import model.User.CashAccount;
import model.User.UserPortfolio;
import sun.applet.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.lang.management.ManagementFactory;

/**
 * Created by byronzaharako on 3/10/16.
 */
public class CashAccountsView extends View {

    private JComboBox<Object> accountSelector, fromAccountSelector, toAccountSelector;
    private JButton withdrawBtn, depositBtn, deleteBtn, addCCBtn, transferButton;
    private JTextField amountField, newAmountField, newNameField, transferAmount;
    private UserPortfolio user;
    private Window win;
    private JLabel balance, transferLabel;

    private int box1SelectedIndex;
    private int fromBoxSelectedIndex;
    private int toBoxSelectedIndex;

    public CashAccountsView(Window window, UserPortfolio userProfile) {
        this.win = window;
        this.user = userProfile;


        JPanel panel = new JPanel(new BorderLayout());

        NavigationPanel nav = new NavigationPanel(win);
        panel.add(nav, BorderLayout.PAGE_START);

        JPanel interactionBoxLayout = new JPanel();
        interactionBoxLayout.setLayout(new BoxLayout(interactionBoxLayout, BoxLayout.Y_AXIS));
        interactionBoxLayout.setBorder(BorderFactory.createLineBorder(new Color(0,0,0)));

        JPanel interactFlowLayout1 = new JPanel();
        interactFlowLayout1.setLayout(new FlowLayout(FlowLayout.LEFT));

        JPanel interactFlowLayout2 = new JPanel();
        interactFlowLayout2.setLayout(new FlowLayout(FlowLayout.LEFT));

        JPanel createCCLayoutFlow = new JPanel();
        createCCLayoutFlow.setLayout(new FlowLayout(FlowLayout.LEFT));
        createCCLayoutFlow.setBorder(BorderFactory.createLineBorder(new Color(0,0,0)));

        JPanel transferBoxLayout = new JPanel();
        transferBoxLayout.setLayout(new BoxLayout(transferBoxLayout, BoxLayout.Y_AXIS));
        transferBoxLayout.setBorder(BorderFactory.createLineBorder(new Color(0,0,0)));

        JPanel transferGroup = new JPanel(new BorderLayout());
        JPanel transferTextInputGroup = new JPanel(new FlowLayout(FlowLayout.CENTER));


        interactFlowLayout1.add(accountSelector = new JComboBox<Object>(user.getCashAccounts().toArray()));
        accountSelector.addActionListener(this);
        interactFlowLayout1.add(balance = new JLabel("Balance = $" + user.getSelectedCashAccount().getCash()));
        interactFlowLayout1.add(depositBtn = new JButton("Deposit Cash"));
        depositBtn.addActionListener(this);
        interactFlowLayout1.add(withdrawBtn = new JButton("Withdraw Cash"));
        withdrawBtn.addActionListener(this);
        interactFlowLayout2.add(new JLabel("Amount: $"));
        interactFlowLayout2.add(amountField = new JTextField());
        amountField.setColumns(15);

        interactFlowLayout2.add(deleteBtn = new JButton("Delete Cash Account"));
        deleteBtn.addActionListener(this);

        createCCLayoutFlow.add(addCCBtn = new JButton("Add a new Cash Account"));
        addCCBtn.addActionListener(this);

        createCCLayoutFlow.add(new JLabel("New Amount: $"));
        createCCLayoutFlow.add(newAmountField = new JTextField());
        newAmountField.setColumns(12);
        createCCLayoutFlow.add(new JLabel("New Cash Account Name:"));
        createCCLayoutFlow.add(newNameField = new JTextField());
        newNameField.setColumns(12);

        transferGroup.add(fromAccountSelector = new JComboBox<Object>(user.getCashAccounts().toArray()), BorderLayout.LINE_START);
        fromAccountSelector.addActionListener(this);
        transferGroup.add(toAccountSelector = new JComboBox<Object>(user.getCashAccounts().toArray()), BorderLayout.LINE_END);
        toAccountSelector.addActionListener(this);


        JPanel transferTitle = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 50 ));
        transferTitle.add(new JLabel("Transfer Cash from the Left Account to the Right Account"));
        transferGroup.add(transferTitle, BorderLayout.PAGE_START);
        transferLabel = new JLabel("$");
        transferAmount = new JTextField("0");
        transferAmount.setColumns(20);
        transferTextInputGroup.add(transferLabel);
        transferTextInputGroup.add(transferAmount);
        transferGroup.add(transferTextInputGroup, BorderLayout.CENTER);
        transferGroup.add(transferButton = new JButton("Transfer"), BorderLayout.PAGE_END);
        transferButton.addActionListener(this);
        transferGroup.setBorder(BorderFactory.createLineBorder(new Color(0,0,0)));


        interactionBoxLayout.add(interactFlowLayout1);
        interactionBoxLayout.add(interactFlowLayout2);

        panel.add(interactionBoxLayout, BorderLayout.LINE_START);
        panel.add(createCCLayoutFlow, BorderLayout.LINE_END);
        panel.add(transferGroup, BorderLayout.PAGE_END);

        this.add(panel);

    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object sauce = actionEvent.getSource();
        if (sauce.equals(accountSelector)) {
            // The account selector changed
            box1SelectedIndex = accountSelector.getSelectedIndex();
            win.commandHandler.selectCashAccount(
                    new SelectCashAccountCommand(
                            win.commandHandler,
                            user.getCashAccounts().get(box1SelectedIndex))
            );
        } else if (sauce.equals(withdrawBtn)) {
            double amount = Double.parseDouble(amountField.getText());
            RemoveCashCommand cmd = new RemoveCashCommand(
                    win.commandHandler,
                    amount);
            win.commandHandler.withdrawCash(cmd);
        } else if (sauce.equals(depositBtn)) {
            double amount = Double.parseDouble(amountField.getText());
            win.commandHandler.addCashToAccount(new AddCashCommand(
                    win.commandHandler,
                    amount,
                    user.getSelectedCashAccount()));
        } else if (sauce.equals(deleteBtn)) {
            win.commandHandler.deleteCashAccount(new DeleteCashAccountCommand(win.commandHandler));
            update();
        } else if (sauce.equals(addCCBtn)) {
            double newAmnt = Double.parseDouble(newAmountField.getText());
            win.commandHandler
                    .createCashAccount(new CreateCashAccountCommand(
                            win.commandHandler,
                            newAmnt,
                            newNameField.getText())
                    );
            update();
        } else if (sauce.equals(transferButton)){
            win.commandHandler.transferCashToOtherAccount(new TransferCashCommand(win.commandHandler,
                    (CashAccount) fromAccountSelector.getSelectedItem(),
                    Double.parseDouble(transferAmount.getText()),
                    (CashAccount) toAccountSelector.getSelectedItem()));
        }
    }

    @Override
    public void update() {
        this.balance.setText("Balance = $" + user.getSelectedCashAccount().getCash());


        accountSelector.setSelectedIndex(box1SelectedIndex);
        toAccountSelector.setSelectedIndex(toBoxSelectedIndex);
        fromAccountSelector.setSelectedIndex(fromBoxSelectedIndex);

        DefaultComboBoxModel newModel = new DefaultComboBoxModel<>(user.getCashAccounts().toArray());
        accountSelector.setModel(newModel);
        accountSelector.revalidate();
        accountSelector.repaint();
        amountField.setColumns(15);
    }
}
