package gui;

import exceptions.WatchListException;
import model.User.Watch.WatchItem;
import model.User.Watch.WatchList;
import gui.TableModels.WatchListTableModel;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created by byronzaharako on 4/3/16.
 */
public class WatchListView extends View implements TableModelListener{

    private static final String REMOVE_TEXT = "Remove WatchList Item";

    WatchList watchList;

    Window win;
    JButton removeButton, resetBtn;
    JPanel panel;


    JTable watchlistTable;

    public WatchListView(Window window){

        win = window;
        panel = new JPanel(new BorderLayout());

        NavigationPanel nav = new NavigationPanel(win);
        panel.add(nav, BorderLayout.PAGE_START); // add the navigation bar

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        buttonPanel.add(removeButton = new JButton(REMOVE_TEXT)); // add the remove button
        removeButton.addActionListener(this);
        buttonPanel.add(resetBtn = new JButton("Reset Alert Trips"));
        resetBtn.addActionListener(this);
        panel.add(buttonPanel, BorderLayout.PAGE_END);

        formatTable(panel);

        JPanel keyPanel = new JPanel();
        keyPanel.setLayout(new BoxLayout(keyPanel, BoxLayout.Y_AXIS));
        keyPanel.add(new JLabel("Key:"));
        keyPanel.add(new JLabel("Green = untripped"));
        keyPanel.add(new JLabel("Yellow = was tripped, but currently isn't"));
        keyPanel.add(new JLabel("Red = currently tripped"));
        panel.add(keyPanel, BorderLayout.EAST);

        //Add everything to the view
        this.add(panel);
    }

    /**
     * Called from the window after the window is told by the model that there was something changed
     */
    @Override
    public void update() {
        formatTable(panel);
    }

    /**
     * Invoked when an action occurs.
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source.equals(removeButton)){
            WatchItem selected = watchList.getItemByTicker(
                    (String) watchlistTable.getValueAt(watchlistTable.getSelectedRow(), 1)
            );
            try {
                watchList.unwatchEquity(selected.getEquity());
                win.setCurrentView(new WatchListView(win));
            } catch (WatchListException e1) {
                e1.printStackTrace();
            }
        } else if (source.equals(resetBtn)) {
            for (WatchItem i: watchList.getWatchList()) {
                i.reset();
                win.setCurrentView(new WatchListView(win));
            }
        }
    }


    public void formatTable(JPanel panel){
        watchList = win.commandHandler.getPortfolioManager().getCurrentUserPortfolio().getWatchList();

        WatchListTableModel tm = new WatchListTableModel(watchList);

        watchlistTable = new JTable(tm) {

            ///// COLOR ??????

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
                Component comp = super.prepareRenderer(renderer, row, col);
                Object value = getModel().getValueAt(row, col);
                String ticker = (String) getModel().getValueAt(row, 1);
                WatchItem item = watchList.getItemByTicker(ticker);
                if (value.equals(0.0)) {
                    comp.setForeground(Color.BLACK);
                    comp.setBackground(Color.WHITE);
                } else if (col == 3) {
                    if (item.lowTripPoint.hasBeenTripped()) {
                        comp.setBackground(Color.RED);
                        comp.setForeground(Color.WHITE);
                    } else if (item.lowTripPoint.hasBeenTripped()) {
                        comp.setBackground(Color.YELLOW);
                        comp.setForeground(Color.BLACK);
                    } else {
                        comp.setBackground(Color.GREEN);
                        comp.setForeground(Color.BLACK);
                    }
                } else if (col == 4) {
                    if (item.highTripPoint.hasBeenTripped()) {
                        comp.setBackground(Color.RED);
                        comp.setForeground(Color.WHITE);
                    } else if (item.highTripPoint.hasBeenTripped()) {
                        comp.setBackground(Color.YELLOW);
                        comp.setForeground(Color.BLACK);
                    } else {
                        comp.setBackground(Color.GREEN);
                        comp.setForeground(Color.BLACK);
                    }
                } else {
                    comp.setBackground(Color.LIGHT_GRAY);
                    comp.setForeground(Color.BLACK);
                }
                return comp;
            }
        };

        watchlistTable.setAutoCreateRowSorter(true); // Auto implements sorting of the table


        JScrollPane scrollPane = new JScrollPane(watchlistTable);
        watchlistTable.setFillsViewportHeight(true);
        scrollPane.setViewportView(watchlistTable);

        panel.add(scrollPane, BorderLayout.CENTER);
    }


    @Override
    public void tableChanged(TableModelEvent tableModelEvent) {
        update(); // I'm unsure about the context under which this is called.
    }
}
