package gui;

import cmd.RedoCommandCommand;
import cmd.ShutdownCommand;
import cmd.UndoCommandCommand;
import com.google.gson.JsonParseException;
import cmd.Handler.CommandHandler;
import model.StorageSubsystem.Storage;
import model.User.UserPortfolioManager;
import model.User.UserPortfolio;
import model.web.MarketUpdater;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.Observable;
import java.util.Observer;
import java.awt.event.*;

/**
 * Main window of the application
 *
 * @author byronzaharako
 */
public class Window extends JFrame implements Observer, ActionListener, KeyListener {

    private View currentView;
    public CommandHandler commandHandler;

    private JMenuItem undoItem;
    private JMenuItem redoItem;
    private JMenuItem exportCurrentPortfolio;
    private JMenuItem importCurrentPortfolio;
    private JMenuItem setMarketUpdateInterval;
    private JMenuItem updateMarketNow;

    /**
     * Create the main window for the app
     *
     */
    public Window(final CommandHandler commandHandler){
        super();
        // Tell the system to save everyting before shutting down
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent event) {
                //commandHandler.getPortfolioManager().saveAllPortfoliosAndShutdown();
                commandHandler.shutdown(new ShutdownCommand(commandHandler));
            }
        });
        this.commandHandler = commandHandler;



        JMenuBar menu = new JMenuBar();
        JMenu fileMenu = new JMenu(("File"));
        exportCurrentPortfolio = new JMenuItem("Export");
        importCurrentPortfolio = new JMenuItem("Import");
        exportCurrentPortfolio.addActionListener(this);
        importCurrentPortfolio.addActionListener(this);
        fileMenu.add(importCurrentPortfolio);
        fileMenu.add(exportCurrentPortfolio);

        menu.add(fileMenu);

        JMenu redoUndoMenu = new JMenu("Actions");
        undoItem = new JMenuItem("undo (ctrl+z)");
        redoItem = new JMenuItem("redo (ctrl+shift+z");
        undoItem.addActionListener(this);
        redoItem.addActionListener(this);
        redoUndoMenu.add(undoItem);
        redoUndoMenu.add(redoItem);

        undoItem.setEnabled(false);
        redoItem.setEnabled(false);

        menu.add(redoUndoMenu);

        JMenu updateMarketMenu = new JMenu("Market Updates");
        setMarketUpdateInterval = new JMenuItem("Set Auto Update Interval");
        updateMarketNow         = new JMenuItem("Update Market Now");
        setMarketUpdateInterval.addActionListener(this);
        updateMarketNow.addActionListener(this);
        updateMarketMenu.add(updateMarketNow);
        updateMarketMenu.add(setMarketUpdateInterval);

        menu.add(updateMarketMenu);

        this.addKeyListener(this);
        this.setFocusable(true);
        this.setJMenuBar(menu);

    }

    public void setCurrentView(View currentView) {
        this.getContentPane().removeAll();
        this.getContentPane().add(currentView);
        this.pack();
        this.currentView = currentView;
        this.currentView.addKeyListener(this);

    }

    public View getCurrentView(){
        return currentView;
    }

    /**
     * Called whenever something changes in the model
     * @param sys The UserPortfolioManager that this observes.
     * @param o the object??
     */
    public void update(Observable sys, Object o) {
        if (commandHandler.Error != null) {
            JOptionPane.showMessageDialog(null,commandHandler.Error.getMessage(),"Oops",JOptionPane.ERROR_MESSAGE);
            commandHandler.Error = null;
            return;
        }
        //Set the actions as doable or not.
        boolean redoPossible = (commandHandler.getCommandHistoryManager().getRedoStackSize() > 0);
        boolean undoPossible = (commandHandler.getCommandHistoryManager().getUndoStackSize() > 0);
        //System.out.println(commandHandler.getUndoStackSize() + " possible commands to redo");
        redoItem.setEnabled(redoPossible);
        undoItem.setEnabled(undoPossible);

        //If the user is not logged in, then grey out the export button, because there is nothing selected to export
        exportCurrentPortfolio.setEnabled(commandHandler.getPortfolioManager().getCurrentUserPortfolio() != null);

        if (commandHandler.getPortfolioManager().getCurrentUserPortfolio() == null) {
            //User logged out
            setCurrentView(new LoginView(this));
        }

        currentView.update();
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        Object source = event.getSource();
        final JFileChooser fc = new JFileChooser();

        if (source.equals(undoItem)){
            commandHandler.undoCommand(new UndoCommandCommand(commandHandler));
        } else if (source.equals(redoItem)){
            commandHandler.redoCommand(new RedoCommandCommand(commandHandler));
        } else if (source.equals(exportCurrentPortfolio)){
            UserPortfolio portfolio = commandHandler.getPortfolioManager().getCurrentUserPortfolio();
            String fileName = portfolio.getUsername()+"Portfolio.json";
            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            //spawn a file chooser
            int returnVal = fc.showSaveDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION){
                File file = fc.getSelectedFile();
                Storage.writePortfolioToFile(portfolio, file);
            }
        } else if (source.equals(importCurrentPortfolio)){
            //spawn a file chooser
            int returnVal = fc.showOpenDialog(this);
            fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
            if (returnVal == JFileChooser.APPROVE_OPTION){
                File file = fc.getSelectedFile();
                UserPortfolioManager sys = commandHandler.getPortfolioManager();
                try {
                    sys.addUserPortfolio(Storage.createPortfolioFromFile(commandHandler.getMarket(),file));
                } catch (JsonParseException e) {
                   System.out.print(e);
                }
            }
        } else if (source.equals(setMarketUpdateInterval)) {
            // Prompt for update time
            String stringTime = JOptionPane.showInputDialog("Enter update time in minutes:");
            try {
                int minutes = Integer.parseInt(stringTime);
                MarketUpdater.setTimer(minutes);
                commandHandler.getPortfolioManager().getCurrentUserPortfolio().setMarketUpdateTime(minutes);
            } catch (NumberFormatException nfe) {
                JOptionPane.showMessageDialog(null, "That is not an integer", "Number Format Exception",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else if (source.equals(updateMarketNow)) {
            System.out.println("Manual market update trigger");
            MarketUpdater.updateMarket(this);
        }

    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
        //Do nothing, I only wanted the keyPressed() method
    }

    /**
     * Handles key events from the user's keyboard in order to enable undo/redo ctrl+[shift]+z functionality.
     * @param keyEvent The key event to detect.
     */
    @Override
    public void keyPressed(KeyEvent keyEvent) {
        System.out.println(keyEvent.getExtendedKeyCode());

        //CTRL+SHIFT+Z to redo
        if ((keyEvent.getKeyCode() == KeyEvent.VK_Z)
                && ((keyEvent.getModifiers() & KeyEvent.CTRL_MASK) != 0)
                && ((keyEvent.getModifiers() & KeyEvent.SHIFT_MASK) != 0)){
            System.out.println("Redo Key Pressed");
            commandHandler.redoCommand(new RedoCommandCommand(commandHandler));
        }
        //CTRL+Z to undo
        else if ((keyEvent.getKeyCode() == KeyEvent.VK_Z)
                && ((keyEvent.getModifiers() & KeyEvent.CTRL_MASK) != 0)
                && ((keyEvent.getModifiers() & KeyEvent.SHIFT_MASK) == 0)){
            System.out.println("Undo Key Pressed");
            commandHandler.undoCommand(new UndoCommandCommand(commandHandler));
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        //Do nothing, I only wanted the keyPressed() method
    }
}
