package gui;

import cmd.BuyEquityCommand;
import cmd.ImportHoldingCommand;
import exceptions.WatchListException;
import gui.TableModels.TradableEntityTableModel;
import gui.TableModels.WatchListTableModel;
import model.MarketStructure.Equity;
import model.MarketStructure.TradableEntity;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by byronzaharako on 3/10/16.
 *
 * Displays the market.
 */
public class MarketView extends View implements TableModelListener{

    private static final String MARKET_TEXT = "Market";
    private static final String SEARCH_TEXT = "Search";
    private static final String AMOUNT_TEXT = "Amount to buy:";
    private static final String BUY_TEXT = "Buy Equity";
    private static final String IMPORT_TEXT = "Import as Holding";



    private static final int MARKET_WIDTH = 650;
    private static final int MARKET_MAX_HEIGHT = 500;
    private static final int MARKET_IDEAL_HEIGHT = 420;

    private Window win;
    private JTextField searchBar;
    private JButton searchButton;
    private JPanel panel;
    private JPanel listPane;
    private JButton buyBtn;
    private JTextField amountToBuyField;
    private JButton importBtn;
    private JButton watchBtn;


    ArrayList<TradableEntity> tradableEntities = new ArrayList<>();
    ArrayList<TradableEntity> staticTradableEntities = new ArrayList<>();


    TradableEntityTableModel tm;
    JTable entityTable;



    public MarketView(Window win) {

        ArrayList<TradableEntity> indexes = win.commandHandler.getMarket().getSelfAndChildren();



        this.win = win;
        for (TradableEntity e : indexes){
            tradableEntities.add(e);
            staticTradableEntities.add(e);
        }

        panel = new JPanel(new BorderLayout(20,0));
        JPanel innerPanel = new JPanel(new BorderLayout());

        NavigationPanel nav = new NavigationPanel(win);
        panel.add(nav, BorderLayout.PAGE_START);


        JPanel searchLayoutFlow = new JPanel();
        searchLayoutFlow.setLayout(new FlowLayout(FlowLayout.CENTER));


        JPanel buyLayoutFlow = new JPanel();
        buyLayoutFlow.setLayout(new FlowLayout(FlowLayout.CENTER));


        searchLayoutFlow.add(searchBar = new JTextField());
        searchBar.setColumns(35);
        searchLayoutFlow.add(searchButton = new JButton(SEARCH_TEXT));
        searchButton.addActionListener(this);
        searchBar.addActionListener(this);

        buyLayoutFlow.add(new JLabel(AMOUNT_TEXT));
        buyLayoutFlow.add(amountToBuyField = new JTextField());
        amountToBuyField.setColumns(12);
        buyLayoutFlow.add(buyBtn = new JButton(BUY_TEXT));
        buyBtn.addActionListener(this);
        buyLayoutFlow.add(importBtn = new JButton(IMPORT_TEXT));
        importBtn.addActionListener(this);
        buyLayoutFlow.add(watchBtn = new JButton("Watch"));
        watchBtn.addActionListener(this);


        //Add the composite sub layouts
        innerPanel.add(searchLayoutFlow, BorderLayout.PAGE_START);
        formatTable(innerPanel, tradableEntities);
        innerPanel.add(buyLayoutFlow, BorderLayout.PAGE_END);

        panel.add(innerPanel, BorderLayout.CENTER);
        // add the top level box layout to the view
        this.add(panel);
    }


    @Override
    public void update() {
        tradableEntities = filterList(staticTradableEntities); //Filter for the desired entities

        tm.tradableEntities = tradableEntities; //Change the model
        entityTable.addNotify();  // Cause the GUI element to update to reflect the change in the model

    }



    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object sauce = actionEvent.getSource();
        if (sauce.equals(buyBtn)){
            try {
                System.out.println(entityTable.getSelectedRow());
                TradableEntity equityToBuy = tradableEntities.get(entityTable.getSelectedRow());
                int amountToBuy = Integer.parseInt(amountToBuyField.getText());
                win.commandHandler.buyEquity(new BuyEquityCommand(
                        win.commandHandler,
                        equityToBuy,
                        amountToBuy));

            } catch (IndexOutOfBoundsException e){
                //This is ugly
                win.commandHandler.Error = new IndexOutOfBoundsException("You must select a equity to buy");
                win.update(win.commandHandler, e);
            }


        } else if (sauce.equals(searchButton) || sauce.equals(searchBar)){
            update();

        } else if (sauce.equals(importBtn)){
            try {
                TradableEntity equityToImport = tradableEntities.get(entityTable.getSelectedRow());
                int amountToImport = Integer.parseInt(amountToBuyField.getText());
                win.commandHandler.importHolding(new ImportHoldingCommand(
                        win.commandHandler,
                        equityToImport,
                        amountToImport));
            } catch (IndexOutOfBoundsException e) {
                win.commandHandler.Error = new IndexOutOfBoundsException("You must select a equity to import");
                win.update(win.commandHandler, e);
            }

        } else if (sauce.equals(watchBtn)){
            TradableEntity entity = tradableEntities.get(entityTable.getSelectedRow());
            try {
                win.commandHandler.getPortfolioManager().getCurrentUserPortfolio().getWatchList().watchEquity(entity);
            } catch (WatchListException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Filter the Equity list for the search box
     */
    private ArrayList<TradableEntity> filterList(ArrayList<TradableEntity> list) {
        ArrayList<TradableEntity> filteredList = new ArrayList<TradableEntity>();
        String filter = this.searchBar.getText();
        if (! filter.equals("")){
            for (TradableEntity h: list) {
                if (h.getName().toLowerCase().contains(filter.toLowerCase())) {
                    filteredList.add(h);
                }
            }
        } else{
            filteredList = staticTradableEntities;
        }
        return filteredList;
    }

    public void formatTable(JPanel panel, ArrayList<TradableEntity> entities){

        tm = new TradableEntityTableModel(entities);

        entityTable = new JTable(tm);
        entityTable.setAutoCreateRowSorter(true); // Auto implements sorting of the table

        entityTable.getModel().addTableModelListener(this);

        JScrollPane scrollPane = new JScrollPane(entityTable);
        entityTable.setFillsViewportHeight(true);


        panel.add(scrollPane, BorderLayout.CENTER);
    }


    @Override
    public void tableChanged(TableModelEvent tableModelEvent) {
        update();
    }
}
