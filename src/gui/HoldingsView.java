package gui;

import cmd.RemoveHoldingCommand;
import cmd.SellHoldingCommand;
import gui.TableModels.HoldingTableModel;
import gui.TableModels.TradableEntityTableModel;
import model.User.Holding;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

/**
 * Created by byronzaharako on 3/13/16.
 */
public class HoldingsView extends View implements TableModelListener {

    private Window win;

    /**
     * the selected element will be at the same index as currentlist
     */
    //private JList holdingList;
    private ArrayList<Holding> currentList;

    private JLabel searchLabel, amountLabel;
    private JTextField searchBox;
    private JTextField amountBox;
    private JButton sellBtn,searchBtn,removeBtn;
    private JLabel totalValueLabel, totalValue;


    HoldingTableModel tm;
    JTable holdingTable;

    public HoldingsView(Window window) {
        win = window;

        JPanel panel = new JPanel(new BorderLayout());
        JPanel innerPanel = new JPanel(new BorderLayout());

        JPanel searchRow = new JPanel();
        searchRow.setLayout(new FlowLayout());

        JPanel actionRow = new JPanel();
        searchRow.setLayout(new FlowLayout());

        NavigationPanel nav = new NavigationPanel(win);
        panel.add(nav, BorderLayout.PAGE_START);


        //innerPanel.add(holdingList = new JList(), BorderLayout.CENTER);
        //Set to logged in users current holdings
        currentList = win.commandHandler.getPortfolioManager().getCurrentUserPortfolio().getHoldings();
//        holdingList.setListData(currentList.toArray());
//        holdingList.setVisibleRowCount(15);

        tm = new HoldingTableModel(currentList);
        holdingTable = new JTable(tm);
        holdingTable.setAutoCreateRowSorter(true); // Auto implements sorting of the table
        holdingTable.getModel().addTableModelListener(this);

        JScrollPane scrollPane = new JScrollPane(holdingTable);
        holdingTable.setFillsViewportHeight(true);


        innerPanel.add(scrollPane, BorderLayout.CENTER);




        searchRow.add(searchLabel = new JLabel("Search: "));
        searchRow.add(searchBox = new JTextField());
        searchBox.setColumns(15);
        searchBox.addActionListener(this);
        searchRow.add(searchBtn = new JButton("Search"));
        searchBtn.addActionListener(this);

        actionRow.add(amountLabel = new JLabel("Desired Number: "));
        actionRow.add(amountBox = new JTextField());
        amountBox.setColumns(5);
        actionRow.add(sellBtn = new JButton("Sell"));
        sellBtn.addActionListener(this);
        actionRow.add(removeBtn = new JButton("Remove"));
        removeBtn.addActionListener(this);

        actionRow.add(totalValueLabel = new JLabel("Total Value:"));
        double total = win.commandHandler.getPortfolioManager().getCurrentUserPortfolio().calculateTotalValue();
        actionRow.add(totalValue = new JLabel("$" + total));

        innerPanel.add(searchRow, BorderLayout.PAGE_START);
        innerPanel.add(actionRow, BorderLayout.PAGE_END);
        panel.add(innerPanel, BorderLayout.CENTER);

        this.add(panel);

    }

    @Override
    public void update() {
        ArrayList<Holding> filtered
                = filterList(win.commandHandler.getPortfolioManager().getCurrentUserPortfolio().getHoldings());
        tm.holdings = filtered;
        holdingTable.addNotify();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object sauce = actionEvent.getSource();
        if (sauce.equals(searchBox) || sauce.equals(searchBtn)) {
           update();
        } else if (sauce.equals(sellBtn)) {
            Holding selected = currentList.get(holdingTable.getSelectedRow());
            win.commandHandler
                    .sellEquity(new SellHoldingCommand(
                            win.commandHandler,
                            selected,
                            Integer.parseInt(amountBox.getText())));
            update();
        } else if (sauce.equals(removeBtn)) {
            Holding selected = currentList.get(holdingTable.getSelectedRow());
            win.commandHandler
                    .removeHolding(new RemoveHoldingCommand(
                            win.commandHandler,
                            selected,
                            Integer.parseInt(amountBox.getText())));
            update();
        }
    }

    /**
     * Filter the Equity list for the search box
     */
    private ArrayList<Holding> filterList(ArrayList<Holding> list) {
        ArrayList<Holding> filteredList = new ArrayList<Holding>();
        String filter = this.searchBox.getText();
        for (Holding h: list) {
            if (h.getTradableEntity().getName().toLowerCase().contains(filter.toLowerCase())) {
                filteredList.add(h);
            }
        }
        return filteredList;
    }

    @Override
    public void tableChanged(TableModelEvent tableModelEvent) {
        //update();
    }
}
