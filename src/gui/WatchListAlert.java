package gui;

import javax.swing.*;

/**
 * Created by byronzaharako on 4/9/16.
 */
public class WatchListAlert {

    private String msg;

    public WatchListAlert(String msg) {
        this.msg = msg;
    }

    public void alertUser() {
        JOptionPane.showMessageDialog(null, msg, "Watchlist Alert", JOptionPane.INFORMATION_MESSAGE);
    }

}
