package gui;

import cmd.CreateUserCommand;
import cmd.LoginCommand;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Login View to allow user to login to the application
 *
 * @author byronzaharako
 */
public class LoginView extends View implements KeyListener{

    private static final int ENTER_KEY_CODE = 10;

    private JLabel usernameLabel, pswdLabel;
    private JTextField usernameField;
    private JPasswordField pswdField;
    private JButton loginBtn;
    private JButton addBtn;
    private Window window;

    public LoginView(Window window) {
        super();
        this.window = window;

        JPanel boxLayoutVert = new JPanel();
        boxLayoutVert.setLayout(new BoxLayout(boxLayoutVert, BoxLayout.Y_AXIS));
        boxLayoutVert.setBorder(BorderFactory.createLineBorder(new Color(0,0,0)));

        JPanel usernameRow = new JPanel();
        usernameRow.setLayout(new FlowLayout());
        JPanel passwordRow = new JPanel();
        passwordRow.setLayout(new FlowLayout());
        JPanel buttonRow = new JPanel();
        buttonRow.setLayout(new FlowLayout());


        usernameRow.add(this.usernameLabel = new JLabel("Username:"));
        usernameRow.add(this.usernameField = new JTextField());
        usernameField.setColumns(15);
        passwordRow.add(this.pswdLabel = new JLabel("Password:"));
        passwordRow.add(this.pswdField = new JPasswordField());
        pswdField.setColumns(15);
        buttonRow.add(this.loginBtn = new JButton("Login"));
        buttonRow.add(this.addBtn = new JButton("Create User"));
        loginBtn.addActionListener(this);
        addBtn.addActionListener(this);

        usernameField.addKeyListener(this);
        pswdField.addKeyListener(this);

        //add the layouts
        boxLayoutVert.add(usernameRow);
        boxLayoutVert.add(passwordRow);
        boxLayoutVert.add(buttonRow);
        this.add(boxLayoutVert);

    }

    /**
     * Called when the button is pressed
     */
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() == loginBtn) {
            String username = usernameField.getText();
            String pswd = new String(pswdField.getPassword());
            window.commandHandler.login(new LoginCommand(
                    window.commandHandler,
                    username,
                    pswd));
        }
        if (actionEvent.getSource() == addBtn) {
            window.commandHandler.createUser(
                    new CreateUserCommand(
                            window.commandHandler,
                            usernameField.getText(),
                            new String(pswdField.getPassword()))
            );
        }
    }

    @Override
    public void update() {
        if (window.commandHandler.getPortfolioManager().getCurrentUserPortfolio() != null) {
            window.setCurrentView(
                    new MainMenuView(window)
            );
        }
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        //Enter key pressed
        if(keyEvent.getExtendedKeyCode() == ENTER_KEY_CODE){
            String username = usernameField.getText();
            String pswd = new String(pswdField.getPassword());
            window.commandHandler.login(new LoginCommand(
                    window.commandHandler,
                    username,
                    pswd));

        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }
}
