package gui;


import model.Transactions.Transaction;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

/**
 * Created by byronzaharako on 3/13/16.
 *
 * View that shows the Transaction History
 */
public class TransactionHistoryView extends View {
    private final static String BACK_TEXT = "Bark";

    private Window win;
    JTextArea area;


    public TransactionHistoryView(Window win) {
        this.win = win;

        JPanel panel = new JPanel(new BorderLayout());

        NavigationPanel nav = new NavigationPanel(win);
        panel.add(nav, BorderLayout.PAGE_START);

        ArrayList<Transaction> transactions = win.commandHandler.getPortfolioManager().getCurrentUserPortfolio().getTransactions();
        area = new JTextArea();
        area.setColumns(20);
        area.setRows(20);
        for (Transaction t: transactions) {
            area.append(t.toString() +"\n");
        }
        area.setEditable(false);
        panel.add(area, BorderLayout.CENTER);
        this.add(panel);
    }

    @Override
    public void update() {
        ArrayList<Transaction> transactions = win.commandHandler.getPortfolioManager().getCurrentUserPortfolio().getTransactions();
        //Set the area to be empty
        area.setText("");
        for (Transaction t: transactions) {
            area.append(t.toString() +"\n");
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object sauce = actionEvent.getSource();
    }
}
