package gui;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.Observer;

/**
 * Generic GUI Layout, holds the necessary interfaces for all views
 *
 * @author byronzaharako
 */
public abstract class View extends JPanel implements ActionListener {

    /**
     * Called from the window after the window is told by the model that there was something changed
     */
    public abstract void update();

}
