package model.MarketStructure;

import exceptions.EquityNotFoundException;

import java.util.ArrayList;
import java.util.Observable;

/**
 * Created by ziggypop on 3/16/16.
 *
 * A tradable entity that is owned by Market and holds Equities.
 */
public class Sector implements TradableEntity {
    private ArrayList<Equity> equities;
    private String name;

    public Sector(String name){
        equities = new ArrayList<>();
        this.name = name;
    }

    @Override
    public TradableEntity findChildByName(String name) throws EquityNotFoundException {
        if (getName().equals(name)){
            return this;
        }
        for (TradableEntity tradableEntity : equities){
            if (tradableEntity.getName().compareToIgnoreCase(name) == 0){
                return tradableEntity;
            }

            // look at children (If for whatever reason this Sector contains more Sectors or equivalent structures
//            if (tradableEntity.getChildren() != null && shallRecurse){
//                try {
//                    tradableEntity.findChildByName(name, true);
//                } catch (EquityNotFoundException e){
//                    //do nothing.
//                }
//            }
        }
        throw new EquityNotFoundException("Equity with name: \"" + name +"\" not found.");
    }

    @Override
    /**
     *
     * TODO: IMPLEMENT ITERATOR OR VISITOR HERE, because we only want to visit the equity once!
     * Updates the value of the contents of a tradable entity.
     * @param percent the percent to update the children by.
     */
    public void updateValueByPercent(double percent) {
        for (Equity e : equities){
            e.updateValueByPercent(percent);
        }
    }

    /**
     * Adds an already existing entity to the list of equities.
     * //TODO make this throw an exception if this the TE cannot be coerced into an equity.
     */
    public void addChildEquity(TradableEntity tradableEntity) {
        equities.add( (Equity) tradableEntity);
    }

    @Override
    /**
     * Gets the value represented by the Sector
     * This is given by iterating over all of the equities and adding them to a sum
     * @return The sum of the component equities.
     */
    public double getValue() {
        double sum = 0;
        for (Equity e : equities){
            sum += e.getValue();
        }
        return sum;
    }


    @Override
    public String getName() {
        return name;
    }
    public String getFullName(){
        return getName();
    }
    public String getTickerSymbol(){return "";}

    @Override
    public String toString(){
        return String.format("%s :  : %f", name, getValue());
    }

    @Override
    public ArrayList<Equity> getChildren() {
        return equities;
    }

    public ArrayList<TradableEntity> getSelfAndChildren(){
        ArrayList<TradableEntity> tradables = new ArrayList<>();
        tradables.add(this);
        for (Equity e : equities){
            tradables.add(e);
        }
        return tradables;
    }
}
