package model.MarketStructure;

import exceptions.EquityNotFoundException;

import java.util.ArrayList;
import java.util.Observable;

/**
 * Created by ziggypop on 3/13/16.
 *
 * The interface for any entity that can be traded
 * Any object that implements this interface can be used in the Composite Pattern.
 */
public interface TradableEntity {


    TradableEntity findChildByName(String name) throws EquityNotFoundException;
    void updateValueByPercent(double percent);
    //void addChildEquity(TradableEntity tradableEntity);
    double getValue();
    //void setValue(double value);
    String getName();
    String getTickerSymbol();
    ArrayList<? extends TradableEntity> getChildren();
    String getFullName();



    enum SortType{
        Name,
        Value,
        TickerSymbol
    }

}
