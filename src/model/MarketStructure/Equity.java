package model.MarketStructure;

import exceptions.EquityNotFoundException;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by ziggypop on 2/28/16.
 *
 * The Equity class implements the TradableEquity interface to satisfy the Component/Composite pattern.
 * The Equity can be the market as a whole, a given market index, or an equity.
 * Some operations can be preformed on any of the above "types" of tradable entities.
 *
 */
public class Equity extends Observable implements TradableEntity {
    private String tickerSymbol;
    private String name;
    private double value;




    public Equity(String tickerSymbol, String name, String value){
        this.tickerSymbol = tickerSymbol;
        this.name = name;
        this.value = Double.parseDouble(value);
    }



    /**
     * This will be used to create an "Index" Equity.
     * This type of Equity will be the market, or subset of TradableEntities like NASDAQ100
     * @param name The name of the index Equity
     */
    public Equity(String name){
        this.tickerSymbol = "";
        this.name = name;
        this.value = 0; // when not an equity, this value will not be used to calculate value.
    }



    public String getName(){
        return this.name;
    }

//    public boolean compareName(String name){
//        return this.name.equals(name);
//    }

    /**
     * I really don't like returning null.
     * @return
     */
    @Override
    public ArrayList<TradableEntity> getChildren() {
        return null;
    }

    /**
     * Gets the String representing the TickerSymbol.
     * @return Ticker Symbol
     */
    @Override
    public String getTickerSymbol(){
        return tickerSymbol;
    }


    /**
     * @param percent A double representing the percentage. .01 is the same as 1%
     */
    public void updateValueByPercent(double percent){
        this.value += value * percent;
    }


    /**
     * This is part of the expressed interface.
     * Don't call this, you will only get Exceptions.
     * @param name The name of the Equity to be found
     * @return Nothing
     * @throws EquityNotFoundException This method will only throw this exception.
     */
    public TradableEntity findChildByName(String name) throws EquityNotFoundException{
        throw new EquityNotFoundException("Equity not found.");
    }

    /**
     * Gets the value of a single share of the equity.
     * @return The value of a single share of the equity.
     */
    public double getValue() {
        return value;
    }

    /**
     * Sets the value of an equity to a new value.
     */
    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString(){
        return String.format("%s : %s : $%.2f", name, tickerSymbol, getValue());
    }

    /**
     * Get the full name of the equity.
     * @return A string comprising of the ticker symbol and then the name.
     */
    public String getFullName(){
        return String.format("%s : %s", tickerSymbol, name);
    }






}
