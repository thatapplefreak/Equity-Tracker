package model.MarketStructure;

import exceptions.EquityNotFoundException;

import java.util.ArrayList;
import java.util.Observable;

/**
 * Created by ziggypop on 3/16/16.
 * Extends tradable entity.
 * This is the root of the market.
 */
public class Market implements TradableEntity {
    ArrayList<Sector> sectors;
    public String name = "Market";
    public String tickerSymbol = "";

    public Market(){
        sectors = new ArrayList<>();
    }


    @Override
    public TradableEntity findChildByName(String name) throws EquityNotFoundException {

        if (getName().equals(name)){
            return this;
        }
        for (TradableEntity tradableEntity : sectors){
            if (tradableEntity.getName().equals(name)){
                return tradableEntity;
            }
            // look at children if allowed to
            if (tradableEntity.getChildren() != null){
                try {
                    return tradableEntity.findChildByName(name);
                } catch (EquityNotFoundException e){
                    //do nothing.
                }
            }
        }
        throw new EquityNotFoundException("Equity with name: \"" + name +"\" not found.");
    }

    public Equity findEquityByTickerSymbol(String tickerSymbol) throws EquityNotFoundException {
        for (Sector sector : getSectors()) {
            for (Equity equity : sector.getChildren()) {
                if (equity.getTickerSymbol().equals(tickerSymbol)) {
                    return equity;
                }
            }
        }
        throw new EquityNotFoundException("No equity with tickerSymbol: " + tickerSymbol);
    }

    /**
     * Update the price of an equity
     * @param tickerSymbol Name of equity
     * @param priceString price as string
     */
    public void updateEquityValueByPrice(String tickerSymbol, String priceString) {
        try {
            Equity equity = findEquityByTickerSymbol(tickerSymbol);
            double price = Double.parseDouble(priceString);
            equity.setValue(price);
        } catch (EquityNotFoundException e) {
            e.printStackTrace();
        }
    }


    @Override
    /**
     *
     * Updates the value of the contents of a tradable entity.
     * @param percent the percent to update the children by.
     */
    public void updateValueByPercent(double percent) {
        for (Sector s : sectors){
            s.updateValueByPercent(percent);
        }
    }

    /**
     * Adds an already existing entity to the list of equities.
     */
    public void addChildEquity(TradableEntity tradableEntity) {
        sectors.add( (Sector) tradableEntity);
    }

    @Override
    /**
     * Gets the value represented by the Sector
     * This is given by iterating over all of the equities and adding them to a sum
     * @return The sum of the component equities.
     */
    public double getValue() {
        double sum = 0;
        for (Sector s : sectors){
            sum += s.getValue();
        }
        return sum;
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getTickerSymbol() {
        return "";
    }


    @Override
    public ArrayList<Sector> getChildren() {
        return sectors;
    }

    public ArrayList<TradableEntity> getSelfAndChildren(){
        ArrayList<TradableEntity> tradables = new ArrayList<>();
        tradables.add(this);
        for (Sector s : sectors){
            tradables.addAll(s.getSelfAndChildren());
        }
        return tradables;
    }

    @Override
    public String toString(){
        return String.format("%s : %s : %f", name, tickerSymbol, getValue());
    }
    public String getFullName(){
        return name;
    }


    public ArrayList<Sector> getSectors(){
        return sectors;
    }

    /**
     * Gets a sector based on the sector's name
     * @param name The name you are looking for.
     * @return A sector that has the name you are looking for.
     * @throws EquityNotFoundException
     */
    public Sector getSectorByName(String name) throws EquityNotFoundException{
        for (Sector sector : sectors) {
            if (sector.getName().equals(name)) {
                return sector;
            }
        }
        throw new EquityNotFoundException("sector not found");
    }

    /**
     * Gets all equities in the the market
     * @return An ArrayList of all equities
     */
    public ArrayList<Equity> getAllEquities() {
        ArrayList<Equity> equities = new ArrayList<>();

        for (Sector sector : getSectors()) {
            for (Equity equity : sector.getChildren()) {
                equities.add(equity);
            }
        }
        return equities;
    }


}
