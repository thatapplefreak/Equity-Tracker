package model.StorageSubsystem;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import model.MarketStructure.Market;
import model.MarketStructure.TradableEntity;
import model.Transactions.Transaction;
import model.User.UserPortfolio;
import model.User.Watch.WatchItem;

import javax.swing.*;
import java.lang.reflect.Type;
import java.io.*;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by ziggypop on 2/28/16.
 *
 * The storage mechanism native to our implementation.
 */
public class Storage {

    final private static String PORTFOLIOS_DATA_PATH = "portfolios.json";
    private static Type userPortfolioListType = new TypeToken<ArrayList<UserPortfolio>>() {}.getType();
    private static Type userPortfolioType = new TypeToken<UserPortfolio>() {}.getType();

    public static boolean saveUserPortfolios(ArrayList<UserPortfolio> portfolios) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Transaction.class, new GsonGenericSerializationAdapter())
                .registerTypeAdapter(WatchItem.class, new GsonWatchItemLinkerSerializationAdapter())
                .registerTypeAdapter(TradableEntity.class, new GsonGenericSerializationAdapter())
                .serializeNulls()
                .create();

        // Convert to JSON
        String portfolioData = gson.toJson(portfolios, userPortfolioListType);
        System.out.println(portfolioData);
        writeStringToFile(portfolioData, new File(PORTFOLIOS_DATA_PATH));

        return true;
    }

    /**
     * Creates an array of userPortfolios from a JSON file.
     * @return ArrayList of UserPortfolios.
     */
    public static ArrayList<UserPortfolio> gsonRestoreUserPortfolios(Market market) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Transaction.class, new GsonGenericSerializationAdapter())
                .registerTypeAdapter(WatchItem.class, new GsonWatchItemLinkerSerializationAdapter(market))
                .registerTypeAdapter(TradableEntity.class, new GsonMarketHoldingLinkerDeserializationAdapter(market))
                .create();

        String data = readStringFromFilename(PORTFOLIOS_DATA_PATH);

        // check for invalid json
        if (!isValidJson(data)) return new ArrayList<>();

        ArrayList<UserPortfolio> portfolios = gson.fromJson(data, userPortfolioListType);

        if (portfolios == null) {
            portfolios = new ArrayList<UserPortfolio>();
        }

        return portfolios;
    }

    /**
     * Creates a single Portfolio from a given file, linking it with the market.
     * @param market The market that the Portfolio should try to link to.
     * @param file The file should contain data for a single portfolio.
     * @return A portfolio that is representative of the contents of the provided file, whose holding's equities are
     *          linked to the given Market.
     * @throws JsonParseException If the parser fails to parse correctly, this will be thrown.
     */
    public static UserPortfolio createPortfolioFromFile(Market market, File file) throws JsonParseException{
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Transaction.class, new GsonGenericSerializationAdapter())
                //.registerTypeAdapter(WatchItem.class, new GsonWatchItemLinkerSerializationAdapter(market))
                .registerTypeAdapter(TradableEntity.class, new GsonMarketHoldingLinkerDeserializationAdapter(market))
                .create();

        String line;
        String data = "";

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(file));
            while ((line = reader.readLine()) != null) {
                data += line;
            }
            reader.close();

            // check for invalid json
            if (!isValidJson(data)){
                throw new JsonParseException("The file failed to parse correctly");
            }
            return gson.fromJson(data, userPortfolioType);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new JsonParseException("The File failed to parse correctly");
    }


    /**
     *
     * @param jsonString The Json String to save.
     * @param file The file to which the function saves.
     * @return a boolean indicating the success or failure to write the file.
     */
    private static boolean writeStringToFile(String jsonString, File file){
        FileWriter writer;
        try {
            writer = new FileWriter(file);
            writer.write(jsonString);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static void writePortfolioToFile(UserPortfolio portfolio, File file){
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Transaction.class, new GsonGenericSerializationAdapter())
                .registerTypeAdapter(TradableEntity.class, new GsonGenericSerializationAdapter())
                .create();

        // Convert to JSON
        String portfolioData = gson.toJson(portfolio, userPortfolioType);
        writeStringToFile(portfolioData, file);
    }

    /**
     * Read String (JSON) from file
     * @param filename File to read
     * @return file content or null if failed
     */
    private static String readStringFromFilename(String filename) {
        String fileContent = "";
        String line;

        try {
            File file = new File(filename);
            BufferedReader reader = new BufferedReader(new FileReader(file));

            while ((line = reader.readLine()) != null) {
                fileContent += line;
            }
            reader.close();
        } catch (IOException e) {
            System.out.println("No storage data found");
            return "";
        }

        return fileContent;
    }

    /**
     * Validates if the json is correct.
     * @param json The json to evaluate.
     * @return True if the json is valid, false if otherwise.
     */
    private static boolean isValidJson(String json) {
        try {
            new JsonParser().parse(json);
        } catch (JsonSyntaxException ex) {
            JOptionPane.showMessageDialog(null, "Data is corrupt!", "Error reading data", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

}
