package model.StorageSubsystem;

import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * Created by ziggypop on 3/24/16.
 *
 * Source of SO-Voodo-Magic:
 * http://stackoverflow.com/questions/3629596/deserializing-an-abstract-class-in-gson#3629771
 *
 * When instansiating a Gson object, use a GsonBuilder and use:
 * .registerTypeAdapter(desiredInterface.class, new GsonGenericSerializationAdapter)
 * .create()
 * to create a Gson object that when storing and restoring, will annotate the JSON with the Class Meta Key,
 * which when restoring, will call the deserialize method found here, detecting the desired class and
 * creating an instance of the inherited type.
 */
public class GsonGenericSerializationAdapter implements
        JsonDeserializer<Object>,
        JsonSerializer<Object>{

    public static final String CLASS_META_KEY = "GENERIC";

    @Override
    /**
     * Deserializes objects from json.
     *
     * @param jsonElement The json to parse.
     * @param type The type.
     * @param jsonDeserializationContext The Context.
     * @return An object that has been restored to it's state by gson.
     */
    public Object deserialize(JsonElement jsonElement,
                                   Type type,
                                   JsonDeserializationContext jsonDeserializationContext)
            throws JsonParseException {
        try{
            JsonObject jsonObj = jsonElement.getAsJsonObject();
            String className = jsonObj.get(CLASS_META_KEY).getAsString();
            Class<?> clz = Class.forName(className);
            return jsonDeserializationContext.deserialize(jsonElement, clz);
        } catch (ClassNotFoundException e){
            throw new JsonParseException(e);
        }
    }

    @Override
    /**
     * Serializes objects from json.
     *
     * @param object The object to serialize.
     * @param type The type.
     * @param jsonSerializationContext The Context.
     * @return An object that has been restored to it's state by gson.
     */
    public JsonElement serialize(Object object,
                                 Type type,
                                 JsonSerializationContext jsonSerializationContext) {
        JsonElement jsonEle = jsonSerializationContext.serialize(object, object.getClass());
        jsonEle.getAsJsonObject().addProperty(CLASS_META_KEY,
                object.getClass().getCanonicalName());
        return jsonEle;
    }


}
