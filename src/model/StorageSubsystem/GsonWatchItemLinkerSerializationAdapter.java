package model.StorageSubsystem;

import com.google.gson.*;
import exceptions.EquityNotFoundException;
import exceptions.WatchListException;
import model.MarketStructure.Equity;
import model.MarketStructure.Market;
import model.MarketStructure.Sector;
import model.MarketStructure.TradableEntity;
import model.User.Watch.WatchItem;

import java.lang.reflect.Type;

/**
 * Created by ziggypop on 4/6/16.
 *
 * Gson was running into a problem with a self-referential caused stack overflow error for WatchItem.
 * I have no idea why that happened (I figured it was because of Observable being implemented, completing the 'loop', but that as a read herring)
 * So this [de]serializer will link the watch items to their tradable entities.
 */
public class GsonWatchItemLinkerSerializationAdapter implements JsonDeserializer<WatchItem>, JsonSerializer<WatchItem>{

    private Market market;
    private static final String NAME = "entityName";

    public GsonWatchItemLinkerSerializationAdapter(Market market){
        this.market = market;
    }


    public GsonWatchItemLinkerSerializationAdapter(){

    }


    @Override
    public WatchItem deserialize(JsonElement jsonElement,
                                 Type type,
                                 JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject jsonObj = jsonElement.getAsJsonObject();
        String name = jsonObj.get("equityName").getAsString();
        Double low = jsonObj.get("low").getAsDouble();
        Double high = jsonObj.get("high").getAsDouble();

        try {
            TradableEntity te = market.findChildByName(name);
            Equity equity = (Equity) te;
            return new WatchItem(equity, low, high );
        } catch (EquityNotFoundException e) {
            throw new JsonParseException("Someone has been editing the storage file, because this is unparsable.");
        } catch (WatchListException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public JsonElement serialize(WatchItem watchItem,
                                 Type type,
                                 JsonSerializationContext jsonSerializationContext) {
        JsonObject obj = new JsonObject();
        obj.addProperty("equityName", watchItem.equity.getName()); // Only save the name, because we need to restore it from the market.
        obj.addProperty("low", watchItem.lowTripPoint.getTripPrice()); // Just save the trip point, you don't need anything more to restore.
        obj.addProperty("high", watchItem.highTripPoint.getTripPrice());
        return obj;
    }
}
