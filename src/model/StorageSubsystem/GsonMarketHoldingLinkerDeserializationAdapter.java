package model.StorageSubsystem;

import com.google.gson.*;
import exceptions.EquityNotFoundException;
import model.MarketStructure.Market;
import model.MarketStructure.TradableEntity;

import java.lang.reflect.Type;

/**
 * Created by ziggypop on 3/28/16.
 * This class facilitates deserializing from Json using Gson, tradable entities
 * that, by using the name found in the Json, link to the existing market, so
 * that when a UserPortfolio is restored, the equities that the UP's Holdings hold
 * are the same as the ones in the market.
 * This allows changes made to the market to reflect on the user's holdings as well.
 */
class GsonMarketHoldingLinkerDeserializationAdapter implements JsonDeserializer<TradableEntity> {

    private static final String NAME = "name";
    private Market market;
    GsonMarketHoldingLinkerDeserializationAdapter(Market market){
        super();
        this.market = market;
    }


    /**
     * Attempt to find the object that exits in the market and link that existing object,
     * so there are not duplicate objects in the user and market.
     * @param jsonElement The Json input to deserialize and link.
     * @param type This isn't used, but is required by the interface.
     * @param jsonDeserializationContext This isn't used, but is required by the interface.
     * @return A tradableEntity that already exists in the Market
     * @throws JsonParseException
     */
    @Override
    public TradableEntity deserialize(JsonElement jsonElement,
                                      Type type,
                                      JsonDeserializationContext jsonDeserializationContext)
            throws JsonParseException {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            String name = jsonObject.get(NAME).getAsString();
        try {
           return market.findChildByName(name);
        } catch (EquityNotFoundException e) {
            //If the TE in the market cannot be found, assume that is was a parsing error and throw that.
            throw new JsonParseException(e);
        }

    }

}
