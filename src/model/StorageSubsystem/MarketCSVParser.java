package model.StorageSubsystem;

import exceptions.EquityNotFoundException;
import exceptions.WebRequestException;
import model.MarketStructure.Equity;
import model.MarketStructure.Market;
import model.MarketStructure.Sector;
import model.web.WebQuote;
import model.web.WebRequestBuilder;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;

/**
 * Created by ziggypop on 2/28/16.
 *
 * Parses a CSV file to create a market ( of TradableEntities).
 */
public class MarketCSVParser {

    private static final int TICKER_SYMBOL_INDEX = 0;
    private static final int EQUITY_NAME_INDEX = 1;
    private static final int EQUITY_VALUE_INDEX = 2;
    private static final int PARENT_ONE_INDEX = 3;
    private static final int PARENT_TWO_INDEX = 4;


    /**
     * Creates THE MARKET.
     * @param pathToFile The path to the file that the parser will operate on.
     * @return The Market: A Equity that contains TradableEntities that represent MarketIndices.
     *          Those market indices hold equities.
     */
    public static Market createMarket(String pathToFile){

        Market market = new Market();

        File csvData = new File(pathToFile);

        try {
            CSVParser parser = CSVParser.parse(csvData, Charset.defaultCharset(), CSVFormat.RFC4180);
            for (CSVRecord csvRecord : parser) {
                // Create a new equity
                Equity newEquity = new Equity(csvRecord.get(TICKER_SYMBOL_INDEX),
                        csvRecord.get(EQUITY_NAME_INDEX),
                        csvRecord.get(EQUITY_VALUE_INDEX));

                try {
                    //if the sector already exists, add the new equity to it
                    Sector existingSector = market.getSectorByName(csvRecord.get(PARENT_ONE_INDEX));
                    if (csvRecord.get(PARENT_ONE_INDEX).equals(existingSector.getName())){
                        existingSector.addChildEquity(newEquity);
                    }
                } catch (EquityNotFoundException e){
                    //Because the index level equity was not found, we will create a new one and add it to the market
                    Sector newIndexTradableEntity = new Sector(csvRecord.get(PARENT_ONE_INDEX));
                    // add the new equity to the new index
                    newIndexTradableEntity.addChildEquity(newEquity);
                    //Add the new index to the market
                    market.addChildEquity(newIndexTradableEntity);
                }
                // try to add to the second sector if it is present.
                try {
                    if (csvRecord.get(PARENT_TWO_INDEX) != null) {
                        try {
                            Sector existingTradableEntityIndex =  market.getSectorByName(csvRecord.get(PARENT_TWO_INDEX));
                            if (csvRecord.get(PARENT_ONE_INDEX).equals(existingTradableEntityIndex.getName())) {
                                existingTradableEntityIndex.addChildEquity(newEquity);
                            }

                        } catch (EquityNotFoundException e) {
                            //Because the index level equity was not found, we will create a new one and add it to the market
                            Sector sector2 = new Sector(csvRecord.get(PARENT_TWO_INDEX));
                            // add the new equity to the new index
                            sector2.addChildEquity(newEquity);
                            //Add the new index to the market
                            market.addChildEquity(sector2);
                        }
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    // no second market listed
                }

            }
        } catch (IOException e){
            //The file was not formatted correctly.
            System.out.println(e.toString());
        }

        return market;
    }
}
