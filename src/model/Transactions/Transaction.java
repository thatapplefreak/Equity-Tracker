package model.Transactions;

/**
 * Created by ziggypop on 3/18/16.
 * This is only useful for the purpose of creating collection of generic transactions.
 *
 * Transactions for the most part are created and read.
 * So only toString and a constructor are required.
 */
public interface Transaction {
}
