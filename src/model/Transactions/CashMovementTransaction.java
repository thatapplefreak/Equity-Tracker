package model.Transactions;

import model.User.CashAccount;

/**
 * Created by ziggypop on 3/25/16.
 */
public class CashMovementTransaction implements Transaction {

    private double money;
    private CashAccount account;
    private TransactionAction action;



    /**
     * Constructor for an import CashTransferTransaction.
     * @param money The amount of cash that is moved in or out.
     * @param account The account that cash is moved into or out of.
     * @param action The action that the transaction represents.
     */
    public CashMovementTransaction(double money, CashAccount account, TransactionAction action){
        this.money = money;
        this.account = account;
        this.action = action;
    }


    public String toString(){

        switch (action) {
            case Deposit:
                return "Deposited $" + money + " to " + account.getName();
            case Withdraw:
                return "Withdrew $" + money + " from " + account.getName();
            default:
                return "No Action specified, something is terribly wrong";
        }
    }
    public enum TransactionAction {
        Withdraw,
        Deposit
    }
}
