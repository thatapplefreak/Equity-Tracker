package model.Transactions;

import model.User.Holding;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by ziggypop on 2/28/16.
 *
 * Stores a record of a transfer of assets.
 */
public class HoldingTransaction implements Transaction {
    private Holding holding;
    private int number; //The number of holdings that were transferred
    private double currentValue; // the value of the holding at the time of the transaction.
    private Date date;
    private TransactionAction action;

    public Holding getHolding(){
        return holding;
    }

    public int getNumber(){
        return number;
    }

    public double getCurrentValue(){
        return currentValue;
    }

    public Date getDate(){
        return date;
    }

    public TransactionAction getAction(){
        return action;
    }

    public HoldingTransaction(Holding holding,
                              int number,
                              double currentValue,
                              TransactionAction action){
        this.holding = holding;
        this.number = number;
        this.currentValue = currentValue;
        this.action = action;
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        this.date = calendar.getTime();
    }


    /**
     * An enum representing what actions you can take with a holding transaction.
     */
    public enum TransactionAction{
        SELL,
        BUY,
        ACQUIRE,
        REMOVE
    }

    @Override
    public String toString(){
        String actionString = "";
        switch (action){
            case SELL:
                actionString = "sold";
                break;
            case BUY:
                actionString = "bought";
                break;
            case ACQUIRE:
                actionString = "acquired";
                break;
            case REMOVE:
                actionString = "removed";
        }
        String returnString = number + " Shares of " + holding.getTradableEntity().getFullName() + " " + actionString + " on " + date.toString();
        return returnString;
    }
}



