package model.Transactions;

import model.User.CashAccount;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by ziggypop on 3/18/16.
 */
public class CashTransferTransaction implements Transaction{
    private CashAccount fromAccount;
    private CashAccount toAccount;
    private double money;
    private Date date;
    private TransactionAction action;

    public CashAccount getCashAccountOrigin() {
        return fromAccount;
    }
    public CashAccount getCashAccountDestination() {
        return toAccount;
    }
    public double getMoney(){
        return money;
    }
    public Date getDate(){
        return date;
    }
    public TransactionAction getAction(){
        return action;
    }



    /**
     * Constructor for a transfer CashTransferTransaction
     * @param fromAccount The account where money originates from.
     * @param money The amount of money that is transferred.
     * @param toAccount The account where money is transferred to.
     * @param action The action that is taken.
     */
    public CashTransferTransaction(CashAccount fromAccount,
                                   double money,
                                   CashAccount toAccount,
                                   TransactionAction action){
        this.fromAccount = fromAccount;
        this.money = money;
        this.toAccount = toAccount;
        this.action = action;
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        this.date = calendar.getTime();
    }

    /**
     * An enum representing what actions you can take with a cash transfer.
     */
    public enum TransactionAction{
        Import,
        Remove,
        Transfer,
    }

    @Override
    public String toString(){
        return  "Transferred $" + money + " from " + fromAccount.getName() + " to " + toAccount.getName();

    }

}
