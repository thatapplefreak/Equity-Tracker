package model.web;

import com.google.gson.*;
import exceptions.WebRequestException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ryan on 3/26/16.
 *
 */
public class YahooAPI {

    private String[] queries;
    private String[] symbols;
    private final double max_return = 400;

    public YahooAPI(String[] queries, String[] symbols) {
        this.queries = queries;
        this.symbols = symbols;
    }

    protected ArrayList<WebQuote> getQuotes() throws WebRequestException, IOException {
        // variable checks
        if (queries.length == 0) {
            throw new WebRequestException("Queries cannot be empty");
        }

        if (symbols.length == 0) {
            throw new WebRequestException("Symbols cannot be empty");
        }

        Gson gson = new Gson();
        ArrayList<WebQuote> quotes = new ArrayList<>(symbols.length);

        int symbol_parts = (int) Math.ceil(symbols.length / max_return);

        for (int i = 0; i < symbol_parts; i++) {

            int startIndex = (int) max_return * i;
            int endIndex;

            // Make sure endIndex doesn't go past symbols.length
            if (symbols.length - startIndex >= max_return) {
                endIndex = (int) max_return * (i + 1);
            } else {
                endIndex = symbols.length;
            }
            String[] split_symbols =  Arrays.copyOfRange(symbols, startIndex, endIndex);

            String jsonResultString = makeRequest(split_symbols);

            JsonElement jsonElement = new JsonParser().parse(jsonResultString);
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            JsonObject queryObject = jsonObject.getAsJsonObject("query");
            JsonObject resultsObject = queryObject.get("results").getAsJsonObject();

            // "quote" element is only an array if 2+ symbols are requested
            if (split_symbols.length == 1) {
                JsonObject quoteObject = resultsObject.get("quote").getAsJsonObject();
                WebQuote quote = gson.fromJson(quoteObject, WebQuote.class);
                quotes.add(quote);
                return quotes;
            }

            JsonArray quoteArray = resultsObject.getAsJsonArray("quote");

            for (JsonElement quoteElement : quoteArray) {
                JsonObject quoteObject = quoteElement.getAsJsonObject();
                WebQuote quote = gson.fromJson(quoteObject, WebQuote.class);
                quotes.add(quote);
            }

        }

        return quotes;
    }

    private String makeRequest(String[] symbols_part) throws IOException {
        // Create a URL and open a connection
        URL YahooURL = new URL(buildURL(queries, symbols_part));
        HttpURLConnection con = (HttpURLConnection) YahooURL.openConnection();

        // Set the HTTP Request type method to GET (Default: GET)
        con.setRequestMethod("GET");
        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);

        // Created a BufferedReader to read the contents of the request.
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        return response.toString();
    }

    /**
     * Puts the pieces of the request URL together
     * @param queries Array of queries
     * @param symbols Array of symbols
     * @return valid URL
     */
    private String buildURL(String[] queries, String[] symbols) {
        final String url_base       = "http://query.yahooapis.com/v1/public/yql?q=select%20";
        final String url_from_where = "%20from%20yahoo.finance.quotes%20where%20symbol%20in%20%28";
        final String url_suffix     = "%29&env=store://datatables.org/alltableswithkeys&format=json";

        return url_base + queriesArrayToFormattedString(queries) + url_from_where +
                symbolsArrayToFormattedString(symbols) + url_suffix;
    }

    /**
     * Set the queries array to the format required for yql request
     * @param queries String[] of queries
     * @return CSV style string of queries
     */
    private String queriesArrayToFormattedString(String[] queries) {
        String formattedString = "";

        for (String query : queries) {
            formattedString += query + ",";
        }

        // remove last comma
        formattedString = formattedString.substring(0, formattedString.length()-1);

        return formattedString;
    }

    /**
     * Change the symbols array to the format required for yql request
     * @param symbols String[] of symbols
     * @return YQL formatted symbols string
     */
    private String symbolsArrayToFormattedString(String[] symbols) {
        String formattedString = "";

        for (String symbol : symbols) {
            formattedString += "%22" + symbol + "%22,";
        }

        // remove last comma
        formattedString = formattedString.substring(0, formattedString.length()-1);

        return formattedString;
    }

    public String[] getQueries() {
        return queries;
    }

    public void setQueries(String[] queries) {
        this.queries = queries;
    }

    public String[] getSymbols() {
        return symbols;
    }

    public void setSymbols(String[] symbols) {
        this.symbols = symbols;
    }
}
