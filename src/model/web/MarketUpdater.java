package model.web;

import exceptions.EquityNotFoundException;
import exceptions.WebRequestException;
import gui.MarketView;
import gui.Window;
import model.MarketStructure.Equity;
import model.MarketStructure.Market;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by ryan on 4/1/16.
 *
 * Handles updating market equities from web request
 */
public class MarketUpdater {

    private static Market market;
    private static WebTimer webTimer;

    /**
     * Iterates through all equities and updates value from quotes
     * @throws EquityNotFoundException
     */
    private static void updateEquitiesFromWebRequest() throws IOException, EquityNotFoundException {

        String[] queries = {"symbol", "LastTradePriceOnly" };

        try {
            MarketIterator marketIterator = new MarketIterator(market.getAllEquities());
            String[] symbols = getSymbolsFromEquities(market);

            ArrayList<WebQuote> quotes = new WebRequestBuilder().queries(queries).symbols(symbols).get();

            while (marketIterator.hasNext()) {
                Equity equity = marketIterator.next();
                String tickerSymbol = equity.getTickerSymbol().trim();

                // filter out symbols with '^' for now...
                if (! tickerSymbol.contains("^")) {
                    WebQuote quote = findWebQuoteByTickerSymbol(quotes, tickerSymbol);

                    String priceString = quote.getLastTradePriceOnly();

                    if (priceString != null) {
                        double newPrice = Double.parseDouble(priceString);
                        equity.setValue(newPrice);
                    }
                }




            }

        } catch (WebRequestException wre) {
            wre.printStackTrace();
        }
    }

    /**
     * Gets the list of symbols from existing equities in the market
     * @param market the market
     * @return list of symbols
     */
    private static String[] getSymbolsFromEquities(Market market) {
        MarketIterator marketIterator = new MarketIterator(market.getAllEquities());
        ArrayList<String> symbolsArray = new ArrayList<>();

        while (marketIterator.hasNext()) {
            Equity equity = marketIterator.next();
            String symbol = equity.getTickerSymbol().trim();

            // filter out symbols with '^' for now...
            if (!symbol.contains("^")) {
                symbolsArray.add(symbol);
            }

        }

        return symbolsArray.toArray(new String[symbolsArray.size()]);
    }

    /**
     * Finds a webquote with the matching ticker symbol
     * @param tickerSymbol The ticker symbol
     * @return The matching WebQuote object
     * @throws EquityNotFoundException
     */
    private static WebQuote findWebQuoteByTickerSymbol(ArrayList<WebQuote> quotes, String tickerSymbol)
            throws EquityNotFoundException {
        for (WebQuote webQuote : quotes) {
            if (webQuote.getSymbol().equals(tickerSymbol)) {
                return webQuote;
            }
        } throw new EquityNotFoundException("No equity with ticker \"" + tickerSymbol + "\"");
    }

    /**
     * Creates a timer to auto update the market on an interval
     * Does not immediately update when created, only starts after specified minutes
     * @param minutes time interval in minutes
     */
    public static void setTimer(int minutes) {
        if (webTimer != null) {
            cancelTimer();
        }
        webTimer = new WebTimer(minutes);
    }

    /**
     * Set the market that is getting updated
     * @param market the market
     */
    public static void setMarket(Market market) {
        MarketUpdater.market = market;
    }

    /**
     * Cancel the WebTimer if needed
     */
    public static void cancelTimer() {
        webTimer.cancel();
    }

    /**
     * Updates the market on a separate thread
     */
    public static void updateMarket() {
       Thread updateThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    updateEquitiesFromWebRequest();
                } catch (IOException e) {
                    System.out.println("IO Exception fetching from Yahoo API:");
                    e.printStackTrace();
                } catch (EquityNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        updateThread.setDaemon(true);
        updateThread.start();
    }

    /**
     * Updates the market on a separate thread
     * This will update the marketview
     */
    public static void updateMarket(final Window window) {
        Thread updateThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    updateEquitiesFromWebRequest();
                    if (window.getCurrentView() instanceof MarketView){
                        window.notify();// notify the window if it is the marketView
                    }
                } catch (IOException e) {
                    System.out.println("IO Exception fetching from Yahoo API:");
                    e.printStackTrace();
                } catch (EquityNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        updateThread.setDaemon(true);
        updateThread.start();
    }

    /**
     * Custom iterator for the market
     */
    public static class MarketIterator implements Iterator<Equity> {

        private ArrayList<Equity> equities;
        private int index = 0;

        public MarketIterator(ArrayList<Equity> equities) {
            this.equities = equities;
        }

        @Override
        public boolean hasNext() {
            return index < equities.size();
        }

        @Override
        public Equity next() {
            return equities.get(index++);
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
