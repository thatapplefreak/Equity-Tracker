package model.web;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ryan on 3/30/16.
 * Automatically makes Yahoo API requests
 * at a user specified interval
 */
public class WebTimer {
    Timer timer;

    int milliToMinute = 60000;

    public WebTimer(int minutes) {
        timer = new Timer();

        if (minutes <= 0) { minutes = 1; } // non zero checks

        // initial delay matches period so that it doesn't fire if the client changes the timer time repeatedly
        timer.schedule(new UpdateTask(), minutes * milliToMinute, minutes * milliToMinute);
    }

    public void cancel() {
        timer.cancel();
    }

    class UpdateTask extends TimerTask {
        @Override
        public void run() {
            System.out.println("Auto updating market...");
            MarketUpdater.updateMarket();
        }
    }
}
