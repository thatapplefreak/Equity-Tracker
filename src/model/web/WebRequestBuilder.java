package model.web;

import exceptions.WebRequestException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ryan on 3/26/16.
 *
 * A builder class for YahooAPI
 */
public class WebRequestBuilder {

    private boolean log = false; // don't log by default

    private String[] queries = {};
    private String[] symbols = {};

    public ArrayList<WebQuote> get() throws WebRequestException, IOException {
        YahooAPI api = new YahooAPI(queries, symbols);
        ArrayList<WebQuote> quotes = api.getQuotes();

        if (log) {
            System.out.println("Found quotes:");
            for (WebQuote quote : quotes) {
                System.out.println(quote.toString());
            }
        }

        return quotes;
    }

    /**
     * Set symbols for request
     * @param symbols List of symbols
     * @return this
     */
    public WebRequestBuilder symbols(String[] symbols) {
        this.symbols = symbols;

        if (log) { System.out.println("Using symbols (" + symbols.length + "): " + Arrays.toString(symbols)); }

        return this;
    }

    /**
     * Set queries for request
     * @param queries List of queries
     * @return this
     */
    public WebRequestBuilder queries(String[] queries) {
        this.queries = queries;

        if (log) { System.out.println("Using queries: " + Arrays.toString(queries)); }

        return this;
    }

    /**
     * Turn logging on during building
     * Call this before other methods you want logged
     * @return This WebRequestBuilder
     */
    public WebRequestBuilder log() {
        this.log = true;

        System.out.println("Logging enabled for WebRequestBuilder");

        return this;
    }
}
