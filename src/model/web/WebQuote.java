package model.web;

/**
 * Created by ryan on 3/26/16.
 * <p/>
 * An object returned from web request
 */
public class WebQuote {
    private String symbol;
    private String Ask;
    private String AverageDailyVolume;
    private String Bid;
    private String AskRealtime;
    private String BidRealtime;
    private String BookValue;
    private String Change_PercentChange;
    private String Change;
    private String Commission;
    private String Currency;
    private String ChangeRealtime;
    private String AfterHoursChangeRealtime;
    private String DividendShare;
    private String LastTradeDate;
    private String TradeDate;
    private String EarningsShare;
    private String ErrorIndicationreturnedforsymbolchangedinvalid;
    private String EPSEstimateCurrentYear;
    private String EPSEstimateNextYear;
    private String EPSEstimateNextQuarter;
    private String DaysLow;
    private String DaysHigh;
    private String YearLow;
    private String YearHigh;
    private String HoldingsGainPercent;
    private String AnnualizedGain;
    private String HoldingsGain;
    private String HoldingsGainPercentRealtime;
    private String HoldingsGainRealtime;
    private String MoreInfo;
    private String OrderBookRealtime;
    private String MarketCapitalization;
    private String MarketCapRealtime;
    private String EBITDA;
    private String ChangeFromYearLow;
    private String PercentChangeFromYearLow;
    private String LastTradeRealtimeWithTime;
    private String ChangePercentRealtime;
    private String ChangeFromYearHigh;
    private String PercebtChangeFromYearHigh;
    private String LastTradeWithTime;
    private String LastTradePriceOnly;
    private String HighLimit;
    private String LowLimit;
    private String DaysRange;
    private String DaysRangeRealtime;
    private String FiftydayMovingAverage;
    private String TwoHundreddayMovingAverage;
    private String ChangeFromTwoHundreddayMovingAverage;
    private String PercentChangeFromTwoHundreddayMovingAverage;
    private String ChangeFromFiftydayMovingAverage;
    private String PercentChangeFromFiftydayMovingAverage;
    private String Name;
    private String Notes;
    private String Open;
    private String PreviousClose;
    private String PricePaid;
    private String ChangeinPercent;
    private String PriceSales;
    private String PriceBook;
    private String ExDividendDate;
    private String PERatio;
    private String DividendPayDate;
    private String PERatioRealtime;
    private String PEGRatio;
    private String PriceEPSEstimateCurrentYear;
    private String PriceEPSEstimateNextYear;
    private String SharesOwned;
    private String ShortRatio;
    private String LastTradeTime;
    private String TickerTrend;
    private String OneyrTargetPrice;
    private String Volume;
    private String HoldingsValue;
    private String HoldingsValueRealtime;
    private String YearRange;
    private String DaysValueChange;
    private String DaysValueChangeRealtime;
    private String StockExchange;
    private String DividendYield;
    private String PercentChange;

    public String getSymbol() {
        return symbol;
    }

    public String getAsk() {
        return Ask;
    }

    public String getAverageDailyVolume() {
        return AverageDailyVolume;
    }

    public String getBid() {
        return Bid;
    }

    public String getAskRealtime() {
        return AskRealtime;
    }

    public String getBidRealtime() {
        return BidRealtime;
    }

    public String getBookValue() {
        return BookValue;
    }

    public String getChange_PercentChange() {
        return Change_PercentChange;
    }

    public String getChange() {
        return Change;
    }

    public String getCommission() {
        return Commission;
    }

    public String getCurrency() {
        return Currency;
    }

    public String getChangeRealtime() {
        return ChangeRealtime;
    }

    public String getAfterHoursChangeRealtime() {
        return AfterHoursChangeRealtime;
    }

    public String getDividendShare() {
        return DividendShare;
    }

    public String getLastTradeDate() {
        return LastTradeDate;
    }

    public String getTradeDate() {
        return TradeDate;
    }

    public String getEarningsShare() {
        return EarningsShare;
    }

    public String getErrorIndicationreturnedforsymbolchangedinvalid() {
        return ErrorIndicationreturnedforsymbolchangedinvalid;
    }

    public String getEPSEstimateCurrentYear() {
        return EPSEstimateCurrentYear;
    }

    public String getEPSEstimateNextYear() {
        return EPSEstimateNextYear;
    }

    public String getEPSEstimateNextQuarter() {
        return EPSEstimateNextQuarter;
    }

    public String getDaysLow() {
        return DaysLow;
    }

    public String getDaysHigh() {
        return DaysHigh;
    }

    public String getYearLow() {
        return YearLow;
    }

    public String getYearHigh() {
        return YearHigh;
    }

    public String getHoldingsGainPercent() {
        return HoldingsGainPercent;
    }

    public String getAnnualizedGain() {
        return AnnualizedGain;
    }

    public String getHoldingsGain() {
        return HoldingsGain;
    }

    public String getHoldingsGainPercentRealtime() {
        return HoldingsGainPercentRealtime;
    }

    public String getHoldingsGainRealtime() {
        return HoldingsGainRealtime;
    }

    public String getMoreInfo() {
        return MoreInfo;
    }

    public String getOrderBookRealtime() {
        return OrderBookRealtime;
    }

    public String getMarketCapitalization() {
        return MarketCapitalization;
    }

    public String getMarketCapRealtime() {
        return MarketCapRealtime;
    }

    public String getEBITDA() {
        return EBITDA;
    }

    public String getChangeFromYearLow() {
        return ChangeFromYearLow;
    }

    public String getPercentChangeFromYearLow() {
        return PercentChangeFromYearLow;
    }

    public String getLastTradeRealtimeWithTime() {
        return LastTradeRealtimeWithTime;
    }

    public String getChangePercentRealtime() {
        return ChangePercentRealtime;
    }

    public String getChangeFromYearHigh() {
        return ChangeFromYearHigh;
    }

    public String getPercebtChangeFromYearHigh() {
        return PercebtChangeFromYearHigh;
    }

    public String getLastTradeWithTime() {
        return LastTradeWithTime;
    }

    public String getLastTradePriceOnly() {
        return LastTradePriceOnly;
    }

    public String getHighLimit() {
        return HighLimit;
    }

    public String getLowLimit() {
        return LowLimit;
    }

    public String getDaysRange() {
        return DaysRange;
    }

    public String getDaysRangeRealtime() {
        return DaysRangeRealtime;
    }

    public String getFiftydayMovingAverage() {
        return FiftydayMovingAverage;
    }

    public String getTwoHundreddayMovingAverage() {
        return TwoHundreddayMovingAverage;
    }

    public String getChangeFromTwoHundreddayMovingAverage() {
        return ChangeFromTwoHundreddayMovingAverage;
    }

    public String getPercentChangeFromTwoHundreddayMovingAverage() {
        return PercentChangeFromTwoHundreddayMovingAverage;
    }

    public String getChangeFromFiftydayMovingAverage() {
        return ChangeFromFiftydayMovingAverage;
    }

    public String getPercentChangeFromFiftydayMovingAverage() {
        return PercentChangeFromFiftydayMovingAverage;
    }

    public String getName() {
        return Name;
    }

    public String getNotes() {
        return Notes;
    }

    public String getOpen() {
        return Open;
    }

    public String getPreviousClose() {
        return PreviousClose;
    }

    public String getPricePaid() {
        return PricePaid;
    }

    public String getChangeinPercent() {
        return ChangeinPercent;
    }

    public String getPriceSales() {
        return PriceSales;
    }

    public String getPriceBook() {
        return PriceBook;
    }

    public String getExDividendDate() {
        return ExDividendDate;
    }

    public String getPERatio() {
        return PERatio;
    }

    public String getDividendPayDate() {
        return DividendPayDate;
    }

    public String getPERatioRealtime() {
        return PERatioRealtime;
    }

    public String getPEGRatio() {
        return PEGRatio;
    }

    public String getPriceEPSEstimateCurrentYear() {
        return PriceEPSEstimateCurrentYear;
    }

    public String getPriceEPSEstimateNextYear() {
        return PriceEPSEstimateNextYear;
    }

    public String getSharesOwned() {
        return SharesOwned;
    }

    public String getShortRatio() {
        return ShortRatio;
    }

    public String getLastTradeTime() {
        return LastTradeTime;
    }

    public String getTickerTrend() {
        return TickerTrend;
    }

    public String getOneyrTargetPrice() {
        return OneyrTargetPrice;
    }

    public String getVolume() {
        return Volume;
    }

    public String getHoldingsValue() {
        return HoldingsValue;
    }

    public String getHoldingsValueRealtime() {
        return HoldingsValueRealtime;
    }

    public String getYearRange() {
        return YearRange;
    }

    public String getDaysValueChange() {
        return DaysValueChange;
    }

    public String getDaysValueChangeRealtime() {
        return DaysValueChangeRealtime;
    }

    public String getStockExchange() {
        return StockExchange;
    }

    public String getDividendYield() {
        return DividendYield;
    }

    public String getPercentChange() {
        return PercentChange;
    }

    @Override
    public String toString() {
        return "Symbol: " + getSymbol() + "\n" +
                "\tName: " + getName() + "\n" +
                "\tLast trade price: " + getLastTradePriceOnly();
    }
}
