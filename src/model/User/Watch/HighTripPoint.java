package model.User.Watch;

import exceptions.WatchListException;
import gui.WatchListAlert;

/**
 * Created by byronzaharako on 4/1/16.
 */
public class HighTripPoint extends TripPoint {

    public HighTripPoint(double highPrice) throws WatchListException {
        super(highPrice);
    }

    @Override
    public void updateFlags(double currentPrice) {
        if (currentPrice > getTripPrice()) {
            if (!isCurrentlyTripped()) {
                WatchListAlert a = new WatchListAlert("An equity has gone over a high price. Please check your watchlist.");
                a.alertUser();
            }
            this.currentlyTripped = true;
            this.hasBeenTripped = true;
        } else {
            this.currentlyTripped = false;
        }
    }
}
