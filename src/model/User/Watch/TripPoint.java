package model.User.Watch;

import exceptions.WatchListException;

/**
 * Created by byronzaharako on 4/1/16.
 */
public abstract class TripPoint {

    protected double tripPrice;

    /**
     * True if currently tripped
     */
    protected boolean currentlyTripped = false;

    /**
     * True if the point has been tripped at any point (and not reset)
     */
    protected boolean hasBeenTripped = false;

    protected TripPoint(double tripPrice) throws WatchListException {
        this.setTripPrice(tripPrice);
    }

    public double getTripPrice() {
        return tripPrice;
    }

    public void setTripPrice(double tripPrice) throws WatchListException {
        if (tripPrice < 0) {
            throw new WatchListException("Cannot enter negative watch price, please enter a positive number");
        }
        this.tripPrice = tripPrice;
    }

    /**
     * Implement the logic to update based on what kind of trip point this is
     */
    public abstract void updateFlags(double currentPrice);

    public boolean isCurrentlyTripped() {
        return currentlyTripped;
    }

    public boolean hasBeenTripped() {
        return hasBeenTripped;
    }

    /**
     * Reset to the initial state of the trip
     */
    public void reset() {
        this.currentlyTripped = false;
        this.hasBeenTripped = false;
    }

}
