package model.User.Watch;

import exceptions.WatchListException;
import model.MarketStructure.Equity;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by byronzaharako on 3/30/16.
 */
public class WatchItem implements Observer{

    //The transient keyword has the effect of preventing GSON from saving the equity.
    public  Equity equity;


    public HighTripPoint highTripPoint;
    public LowTripPoint lowTripPoint;

    public WatchItem(Equity te) {
        try {
            highTripPoint = new HighTripPoint(0);
        } catch (WatchListException e) {
            e.printStackTrace();
        }
        try {
            lowTripPoint = new LowTripPoint(0);
        } catch (WatchListException e) {
            e.printStackTrace();
        }

        te.addObserver(this);

        this.equity = te;
    }

    public WatchItem(Equity te, double low, double high) throws WatchListException {
        this(te);
        setLowTrip(low);
        setHighTrip(high);
    }

    public Equity getEquity() {
        return equity;
    }

    public String getEquityName() {
        return equity.getName();
    }

    public String getTradableEntityTickerSymbol() {

        return equity.getTickerSymbol();
    }

    public double getTradableEntityPrice() {
        return equity.getValue();
    }

    public void setLowTrip(double lowPrice) throws WatchListException {
        if (highTripPoint.getTripPrice() != 0.0 && lowPrice > highTripPoint.getTripPrice()) {
            throw new WatchListException("The low price cannot be higher than the high price. Please select a different number");
        }
        this.lowTripPoint = new LowTripPoint(lowPrice);
    }

    public void setHighTrip(double highPrice) throws WatchListException {
        if (highPrice < lowTripPoint.getTripPrice()) {
            throw new WatchListException("The high price cannot be lower than the low price. Please select a different number");
        }
        this.highTripPoint = new HighTripPoint(highPrice);
    }

    public HighTripPoint getHighTrip(){
        return highTripPoint;
    }
    public LowTripPoint getLowTrip(){
        return lowTripPoint;
    }

    @Override
    public void update(Observable o, Object arg) {
        highTripPoint.updateFlags(equity.getValue());
        lowTripPoint.updateFlags(equity.getValue());
    }

    public void reset() {
        highTripPoint.reset();
        lowTripPoint.reset();
    }
}
