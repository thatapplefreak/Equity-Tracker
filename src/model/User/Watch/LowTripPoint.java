package model.User.Watch;

import exceptions.WatchListException;
import gui.WatchListAlert;

/**
 * Created by byronzaharako on 4/1/16.
 */
public class LowTripPoint extends TripPoint {

    public LowTripPoint(double lowPrice) throws WatchListException {
        super(lowPrice);
    }

    /**
     * Implement the logic to update based on what kind of trip point this is
     */
    @Override
    public void updateFlags(double currentPrice) {
        if (currentPrice < getTripPrice()) {
            if (!isCurrentlyTripped()) {
                WatchListAlert a = new WatchListAlert("An equity has gone under a low price. Please check your watchlist.");
                a.alertUser();
            }
            this.currentlyTripped = true;
            this.hasBeenTripped = true;
        } else {
            this.currentlyTripped = false;
        }
    }
}
