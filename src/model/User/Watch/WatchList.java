package model.User.Watch;

import exceptions.WatchListException;
import model.MarketStructure.Equity;
import model.MarketStructure.TradableEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * List of stocks that the user wants to watch for changes
 * Created by byronzaharako on 3/30/16.
 */
public class WatchList {

    private ArrayList<WatchItem> watching;

    public WatchList() {
        this.watching = new ArrayList<WatchItem>();
    }

    /**
     * Add an equity to watch to the watchlist
     * @return true if the equity is now being watched
     */
    public void watchEquity(TradableEntity tradableEntity) throws WatchListException{
        // Make sure the equity isn't already being tracked
        for (WatchItem w: watching) {
            if (tradableEntity.getName().equals(w.getEquityName())) {
                throw new WatchListException("Equity is already getting tracked in the watchlist");
            }
        }
        if (tradableEntity instanceof Equity) {
            this.watching.add(new WatchItem((Equity) tradableEntity));
        }
    }

    public void unwatchEquity(TradableEntity tradableEntity) throws WatchListException {
        for (int i = watching.size() - 1; i >= 0; i--) {
            WatchItem w = watching.get(i);
            if (tradableEntity.getName().equals(w.getEquityName())) {
                watching.remove(w);
                return;
            }
        }
        throw new WatchListException("Equity isn't being tracked right now");
    }

    public ArrayList<WatchItem> getWatchList(){
        return watching;
    }

    public WatchItem getItemByTicker(String ticker) {
        for (WatchItem item: watching) {
            if (item.getTradableEntityTickerSymbol().equals(ticker)) {
                return item;
            }
        }
        return null;
    }
}
