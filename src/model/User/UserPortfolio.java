package model.User;


import exceptions.CashException;
import exceptions.HoldingNotFoundException;
import exceptions.SharesException;
import model.MarketStructure.Equity;
import model.MarketStructure.TradableEntity;
import model.Transactions.HoldingTransaction;
import model.Transactions.Transaction;
import model.User.Watch.WatchList;

import java.util.ArrayList;

/**
 * Created by ziggypop on 2/28/16.
 *
 * Represents the user portfolio.
 */
public class UserPortfolio {
    private String username;
    private Password password;
    private ArrayList<Holding> holdings;
    private ArrayList<CashAccount> cashAccounts; // The cash accounts associated with this user.
    private ArrayList<Transaction> transactions; // This is the history of holdingTransactions
    private CashAccount selectedCashAccount; // This will be the cash account that is interacted with
    private WatchList watchList;
    private int marketUpdateTime = 5; // default 5 minutes

    public String getUsername(){
        return username;
    }
    public Password getPassword(){
        return password;
    }
    public ArrayList<CashAccount> getCashAccounts() {return cashAccounts;}
    public ArrayList<Holding> getHoldings() { return holdings;}
    public CashAccount getSelectedCashAccount() {return selectedCashAccount;}
    public ArrayList<Transaction> getTransactions(){return transactions;}
    public WatchList getWatchList(){return watchList;}

    /**
     * Sets the CashAccount that all holdingTransactions will take place on.
     * This is done so that when buying and selling entities in the gui,
     * the user will not be required to select the cash account each time.
     * @param desiredCashAccount The cash account that you want to become default.
     */
    public void setSelectedCashAccount(CashAccount desiredCashAccount){
        this.selectedCashAccount = desiredCashAccount;
    }

    public UserPortfolio(String username, String password) {
        this.username = username;
        this.password = new Password(password);
        this.holdings = new ArrayList<Holding>();
        this.cashAccounts = new ArrayList<CashAccount>();
        CashAccount cc = new CashAccount(0, "Default");
        cashAccounts.add(cc);
        setSelectedCashAccount(cc);
        this.transactions = new ArrayList<Transaction>();
        this.watchList = new WatchList();
    }

    public  UserPortfolio(String username, String hashedPass, ArrayList<Holding> holdings,
                          ArrayList<CashAccount> cashAccounts, ArrayList<HoldingTransaction> holdingTransactions) {

    }

    public Holding getHoldingByTradableEntity(Equity tradableEntity) throws HoldingNotFoundException{
        for(Holding holding: holdings){
            if (holding.getTradableEntity().equals(tradableEntity)){
                return holding;
            }
        }
        throw new HoldingNotFoundException("User does not hold an equity of this type");
    }


    /**
     * Adds a holding or adds the number of equities to an existing holding.
     * This will NOT remove money from the cash account.
     * @param tradableEntity Equity to be bought.
     * @param number The number of Equities.
     */
    public void acquireHolding(Equity tradableEntity, int number){
        boolean holdingForEquityExists = false;
        number = Math.abs(number);

        for (Holding holding : holdings){
            if (holding.getTradableEntity().equals(tradableEntity)){
                holdingForEquityExists = true;
                try {
                    holding.addShares(number);
                } catch (SharesException e) {
                    //THIS SHOULD NEVER HAPPEN
                    e.printStackTrace();
                }
                break;
            }
        }

        if (!holdingForEquityExists) {
            Holding newHolding = new Holding(tradableEntity, number);
            HoldingTransaction newHoldingTransaction = new HoldingTransaction(newHolding,
                    number,
                    newHolding.getValue(),
                    HoldingTransaction.TransactionAction.ACQUIRE);
            transactions.add(newHoldingTransaction);
            holdings.add(newHolding);
        }
    }


    /**
     * Adds a holding or adds the number of equities to an existing holding.
     * This will remove money from the cash account.
     * @param tradableEntity Equity to be bought.
     * @param number The number of Equities.
     * @return If the purchase completed successfully or not.
     */
    public boolean purchaseHolding(Equity tradableEntity, int number){


        return true;
    }

    /**
     * Sells the a number of shares from a holding after checking if it is ok.
     * This will add money to the CashAccount
     * @param holding The holding that the equities will be sold from.
     * @param number The number of shares to be sold.
     * @return True if the transaction succeeded, False if the transaction falied
     */
    public boolean sellHolding(Holding holding, int number){
        double cost = holding.getTradableEntity().getValue();
        // Check if by selling a holding, you wont exhaust your cash account (ie, an underwater asset)
        try {
            selectedCashAccount.addCash(cost);
        } catch (CashException e){
            System.out.println(e.toString());
            return false;
        }

        //remove the holding shares
        try {
            holding.removeShares(number);
        } catch (SharesException e){
            System.out.println(e.toString());
            //This is a possible point of failure
            try {
                selectedCashAccount.removeCash(cost);
            } catch (CashException e1) {
                //do nothing
                e1.printStackTrace();
            }
            return false;
        }

        // remove the holding from the portfolio if the holding has 0 shares.
        if (holding.getNumberSharesOwned() == 0) {
            holdings.remove(holding);
        }

        HoldingTransaction newHoldingTransaction = new HoldingTransaction(holding,
                number,
                holding.getValue(),
                HoldingTransaction.TransactionAction.SELL);
        //add the transaction to the portfolio
        transactions.add(newHoldingTransaction);


        return true;
    }

    /**
     * Sells the a number of shares from a holding after checking if it is ok.
     * This will NOT add money to the CashAccount
     * @param holding The holding that the equities will be sold from.
     * @param number The number of shares to be sold.
     */
    public void removeHolding(Holding holding, int number){
        try {
            holding.removeShares(number);
            HoldingTransaction newHoldingTransaction = new HoldingTransaction(holding,
                number,
                holding.getValue(),
                HoldingTransaction.TransactionAction.REMOVE);
            //add the transaction to the portfolio
            transactions.add(newHoldingTransaction);
            if (holding.getNumberSharesOwned() == 0) {
                holdings.remove(holding);
            }
        } catch (SharesException e){
            System.out.println(e.toString());
        }
    }


    /**
     * Creates a CashAccount
     * @param money Amount of money the cash account should have
     * @param name The name of the Cash Account.
     */
    public boolean createCashAccount(double money, String name){
        boolean canCreateNewCC = true;
        CashAccount exsistingCashAccount;
        for(CashAccount cc : cashAccounts){
            if(cc.getName().equals(name)){
                canCreateNewCC = false;
                exsistingCashAccount = cc;
                try {
                    exsistingCashAccount.addCash(money);
                } catch (CashException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        if (canCreateNewCC){
            CashAccount newCashAccount = new CashAccount(money, name);
            cashAccounts.add(newCashAccount);
            System.out.print("new cash account created:" + newCashAccount.getName() +":"+newCashAccount.getCash());
        }

        return canCreateNewCC;
    }


    public double calculateTotalValue(){
        double total = 0;
        for (Holding h : holdings){
            total += h.getValue();
        }
        return total;
    }


    public void addTransaction(Transaction transaction){
        transactions.add(transaction);
    }
    public void addHolding(Holding holding){
        holdings.add(holding);
    }
    public void removedTransactionFromHistory(Transaction transaction){
        transactions.remove(transaction);
    }
    public void deleteHolding(Holding holding){
        holdings.remove(holding);
    }

    public void addNumberOfHolding(TradableEntity te, int number) throws SharesException{
        boolean holdingFound = false;
        if (number == 0){
            throw new SharesException("You need to specify more than 0 shares");
        } else if (number < 0) {
            throw new SharesException("You cannot specify a negative number of shares");
        }
        //iterate through all of the holdings, find the existing one and add a number of shares to it.
        for (Holding h : holdings){
            if (h.getTradableEntity().equals(te)){
                h.addShares(number); //
                holdingFound = true;
                break;
            }
        }
        // if the holding does not already exist, create it and add it back to the collection.
        if (!holdingFound){
            Holding newHolding = new Holding(te,number);
            holdings.add(newHolding);
        }
    }

    public int getMarketUpdateTime() {
        return marketUpdateTime;
    }

    public void setMarketUpdateTime(int marketUpdateTime) {
        if (marketUpdateTime > 0){
            this.marketUpdateTime = marketUpdateTime;
        } else {
            System.out.println("No zero time allowed");
        }
    }
}
