package model.User;

import exceptions.SharesException;
import model.MarketStructure.TradableEntity;


/**
 * Created by ziggypop on 2/28/16.
 *
 */
public class Holding {
    private TradableEntity tradableEntity;
    private int numberOwned;

    /**
     * Create a holding at runtime
     * @param tradableEntity The tradableEntity the holding represents
     * @param numberOwned The number of equities the holding contains
     */
    public Holding(TradableEntity tradableEntity, int numberOwned){
        this.tradableEntity = tradableEntity;
        this.numberOwned = numberOwned;
    }



    /**
     * Gets the value of the holding.
     * This is calculated by multiplying the value of the associated entity and the number owned of that entity.
     * @return The value of the holding.
     */
    public double getValue(){
        return tradableEntity.getValue()
                * numberOwned;
    }

    /**
     * @return Gets the entity associated with the holding.
     */
    public TradableEntity getTradableEntity(){
        return tradableEntity;
    }

    /**
     * @return The number of owned shares of the associated entity.
     */
    public int getNumberSharesOwned(){
        return numberOwned;
    }

    /**
     * Changes the number of shares owned.
     * @param number The number to change.
     * @throws SharesException If the input number is negative, and by adding that number to the owned number,
     *          you create a negative, the exception will be thrown, indicating that you can not do this.
     */
    private void alterShares(int number) throws SharesException {
        if(numberOwned + number < 0){
            throw new SharesException("You cannot remove more shares than you own");
        } else {
            numberOwned += number;
        }
    }

    /**
     * Remove shares
     * @param number A positive number, will remove shares from the account.
     * @throws SharesException
     */
    public void removeShares(int number) throws SharesException {
        alterShares(-number);
    }

    /**
     * Add shares
     * @param number A positive number, will add shares to the account.
     * @throws SharesException
     */
    public void addShares(int number) throws SharesException{
        alterShares(number);
    }

    @Override
    public String toString(){
        return String.format("%s : %d", tradableEntity.toString(),  numberOwned);
    }



}
