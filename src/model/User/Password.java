package model.User;

/**
 * Created by ziggypop on 2/28/16.
 *
 * Represents a password for a user.
 * To create a password in the typical use of the program, pass the plaintext into the constructor.
 * This will hash it and store the hash.
 * You can also create a password directly from the hash for the purpose of restoring the program
 * from storage.
 */
public class Password {
    private String hashedPassword;


    public Password(String plaintext){
        hashedPassword = hashPassword(plaintext);
    }

    public Password createPasswordFromHash(String hash) {
        Password password = new Password("");
        password.hashedPassword = hash;
        return password;
    }
    /**
     * @param plaintext The plaintext for the password
     * @return The plaintext shall be hashed so that it isn't recognizable as itself
     */
    private String hashPassword(String plaintext){
        //The current "hash" is the plaintext reversed
        return new StringBuilder(plaintext).reverse().toString();
    }

    /**
     * Verifies the password
     * @param plaintext The plaintext to be checked
     * @return True if the hashed password and hashed plaintext match, otherwise false
     */
    public boolean verifyPassword(String plaintext){
        return hashedPassword.equals(hashPassword(plaintext));
    }

    @Override
    public String toString(){
        return hashedPassword;
    }

}
