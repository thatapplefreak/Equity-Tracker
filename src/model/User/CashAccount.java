package model.User;

import exceptions.CashException;

/**
 * Created by ziggypop on 2/28/16.
 *
 * Holds the cash for a user.
 */
public class CashAccount {
    private double cash;
    private String name;


    /**
     * Constructor for a CashAccount
     * @param cash The amount of cash.
     * @param name The name of the Cash Account.
     */
    public CashAccount(double cash, String name){
        this.cash = cash;
        this.name = name;
    }

    public String getName(){
        return name;
    }

    /**
     * @return The amount of cash in the cash account.
     */
    public double getCash(){
        return cash;
    }

    /**
     * Alters the amount of cash in the cash account
     * @param amount The amount of cash you wish to change, negative values will remove money from the account.
     * @throws CashException If the cash in the cash account is less than the amount requested to be withdrawn, this exception will be thrown.
     */
    private void alterCash(double amount) throws CashException{
        if (cash + amount < 0){
            throw new CashException("Not enough $$ in the bank");
        }
        cash = cash + amount;
    }

    /**
     * Adds cash to the CashAccount by wrapping alterCash()
     * @param amount The amount of cash to add (a positive double please)
     * @throws CashException
     */
    public void addCash(double amount) throws CashException {
        if (amount < 0){
            throw new CashException("Please input a positive number");
        }
        alterCash(amount);
    }

    /**
     * Removes the cash from the CashAccount by wrapping alterCash()
     * @param amount The amount of cash to remove (a positive double)
     * @throws CashException
     */
    public void removeCash(double amount) throws CashException {
        if (amount < 0){
            throw new CashException("Please input a positive number");
        }
        alterCash(-amount);
    }

    /**
     * Sends cash to another cash account
     * @param other The cash account to which cash will be sent.
     * @param amount The amount of cash to send
     * @throws CashException
     */
    public void sendCashToOtherCashAccount(CashAccount other, double amount) throws CashException {
        try {
            removeCash(amount);
        } catch (CashException e) {
            e.printStackTrace();
            //Throw the exception
            throw e;
        }
        try {
            other.addCash(amount);
        } catch (CashException e) {
            try {
                // add the cash back to this account if you cant add cash to the other account
                addCash(amount);
            } catch (CashException e1) {
                // THIS SHOULD NEVER HAPPEN, the method should throw earlier than this point if this is error is to occur
                e1.printStackTrace();
            }
            e.printStackTrace();
            //Throw the exception
            throw e;
        }

    }


    @Override
    public String toString(){
        return name;
    }


}
