package model.User;

import cmd.*;
import model.MarketStructure.Market;
import model.StorageSubsystem.Storage;
import model.User.UserPortfolio;

import java.util.ArrayList;

/**
 * Created by ziggypop on 2/28/16.

 *
 * Holds the collection of UserPortfolios, and keeps track of the current user.
 * handles logging in and out.
 *
 */
public class UserPortfolioManager {

    public ArrayList<UserPortfolio> portfolios;
    private UserPortfolio currentUserPortfolio;


    /**
     * Creates the CHSS.
     * It will create the portfolios by recreating them from storage.
     * It will create the market by parsing the CSV file we are to operate on.
     * And it creates a simulator with no simulations queued.
     */
    public UserPortfolioManager(Market market) {
        portfolios = Storage.gsonRestoreUserPortfolios(market);
    }



    /**
     * Used by the loginCommand to set the current user
     * @param portfolio The portfolio to which current user portfolio will be set.
     */
    public void setCurrentUserPortfolio(UserPortfolio portfolio) {
        this.currentUserPortfolio = portfolio;
    }

    public void addUserPortfolio(UserPortfolio portfolio){
        portfolios.add(portfolio);
    }

    /**
     * Sets the current user portfolio to null, as part of a logout.
     */
    public void unsetCurrentUserPortfolio(){
        this.currentUserPortfolio = null;
    }

    /**
     * Returns the currently active portfolio.
     * @return The currently active portfolio.
     */
    public UserPortfolio getCurrentUserPortfolio() {
        return this.currentUserPortfolio;
    }

    /**
     * Removes the given user portfolio.
     * @param user The UserPortfolio to be removed.
     */
    public void removeUserPortfolio(UserPortfolio user){
        portfolios.remove(user);
    }


}
