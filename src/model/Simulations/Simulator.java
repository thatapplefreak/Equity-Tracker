package model.Simulations;

import model.MarketStructure.Market;

import java.util.LinkedList;

/**
 * Created by ziggypop on 2/28/16.
 *
 * Simulates the market.
 * Users can add simulations to the simulator and then step through them one by one,
 * or run them all at once.
 */
public class Simulator {

    LinkedList<Simulation> simulations;
    LinkedList<Simulation> runSimulations;
    private Market market;

    public static final double DAY = 0.00273972602;
    public static final double WEEK = 0.01917808219;
    public static final double MONTH = 0.08333333333; 
    public static final double YEAR = 1.0;

    public Simulator(Market market){
        simulations = new LinkedList<>();
        runSimulations = new LinkedList<>();
        this.market = market;
    }
    public LinkedList<Simulation> getSimulations(){
        return simulations;
    }
    public Market getMarket() {
        return market;
    }


    /**
     * Creates and adds a simulation to the simulator.
     * @param simulationType The desired type of simulation.
     * @param duration How long the simulation should run, in fractions of a year.
     * @param perAnumChange How the market should react per year.
     */
    public void addSimulation(Simulation.SimulationEnum simulationType,
                              double duration, double perAnumChange){
       switch (simulationType){
           case Bear:
               BearSimulation bearSimulation = new BearSimulation(duration, perAnumChange, this);
               simulations.add(bearSimulation);
               break;
           case Bull:
               BullSimulation bullSimulation = new BullSimulation(duration, perAnumChange, this );
               simulations.add(bullSimulation);
               break;
           case NoGrowth:
               NoGrowthSimulation noGrowthSimulation = new NoGrowthSimulation(duration, this);
               simulations.add(noGrowthSimulation);
       }

    }

    /**
     * Takes an existing simulation and adds it to the simulator queue.
     * @param simulation The simulation to be added to the simulator queue.
     */
    public void addSimulation(Simulation simulation){
        simulations.add(simulation);
    }


    /**
     * Revert the most recent simulation, removing it from the list and reverting it.
     */
    public Simulation removeMostRecentSimulation(){
        Simulation simulation = simulations.removeLast();
        simulation.revert();
        return simulation;
    }

    /**
     * Reverts all simulations and removes them, ideally restoring the program to its initial state,
     * probably with floating point.
     */
    public void resetSimulations(){
        for (Simulation simulation: simulations){
            simulation.revert();
        }
        simulations.clear();
    }

    /**
     * Will simulate the market; either all at once, or one queued simulation at a time.
     * @param isStep If true, the market will only run the current simulation, and remove it from the collection,
     *               otherwise, all simulations will be applied.
     */
    public void runSimulation(boolean isStep){
        if (isStep){
            if (simulations.size() > 0){
                Simulation activeSimulation = simulations.remove();
                activeSimulation.run();
                runSimulations.add(activeSimulation);
                System.out.println("One Simulation Run");
            }
        } else {
            for (Simulation sim : simulations){
                sim.run();
                runSimulations.add(sim);
            }
            simulations.clear();
            System.out.println("All Simulations Run");
        }
    }


    /**
     * Rolls back simulation(s), restoring the state of the market to what it was before the simulation(s) ran.
     * @param isStep if true, only do one revert; false, revert all
     */
    public void rollBackSimulation(boolean isStep){
        if (isStep){
            if (simulations.size() > 0){
                Simulation activeSimulation = runSimulations.pop();
                activeSimulation.revert();
                simulations.add(activeSimulation);
                System.out.println("One Simulation Run");
            }
        } else {
            for (Simulation sim : simulations){
                sim.revert();
                simulations.add(sim);
            }
            runSimulations.clear();
            System.out.println("All Simulations Run");
        }

    }

    /**
     * Gets the number of simulations queued in the simulator
     * @return The number of simulations that are queued.
     */
    public int getSizeOfSimulation(){
        return simulations.size();
    }



}


