package model.Simulations;

/**
 * Created by ziggypop on 3/11/16.
 *
 * A simulation that allows time to pass without the market changing.
 */
public class NoGrowthSimulation implements Simulation {

    private double duration;
    private Simulator simulator;

    public NoGrowthSimulation(double duration, Simulator simulator){
        this.duration = duration;
        this.simulator = simulator;
    }


    @Override
    public void run() {
        simulator.getMarket().updateValueByPercent(0);
    }

    @Override
    public void revert() {
        simulator.getMarket().updateValueByPercent(0);
    }


    @Override
    public String toString() {
        return "No Growth Simulation for " + duration + "years at a yearly rate of: 0";
    }
}
