package model.Simulations;

/**
 * Created by ziggypop on 3/8/16.
 *
 * An abstract class for a Simulation
 * It should be able to run, effecting the market in an expected manner.
 * It should be able to revert the run action, doing the opposite.
 */
public interface Simulation {

    /**
     * Defines what the simulation should do to the market.
     */
    public abstract void run();

    /**
     * Reverts the action taken by run.
     */
    public abstract void revert();


    public enum SimulationEnum{
        Bear,
        Bull,
        NoGrowth
    }




}
