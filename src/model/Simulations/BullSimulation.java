package model.Simulations;

/**
 * Created by ziggypop on 3/11/16.
 *
 * A simulation of a GOOD economy.
 * The value of the market will rise while this simulation is being run.
 */
public class BullSimulation implements Simulation {

    private double duration;
    private double percent;
    private Simulator simulator;

    /**
     * A simulation simulating a good economy.
     * @param duration The interval the Simulation should run for
     * @param percent A positive double that corresponds to the percent that the user wishes to simulate the market with
     *                .01 is equal to 1 percent
     */
    public BullSimulation(double duration, double percent, Simulator simulator){
        this.duration = duration;
        this.percent = Math.abs(percent);
        this.simulator = simulator;
    }


    @Override
    public void run() {
        double actualChange = percent * duration;
        simulator.getMarket().updateValueByPercent(actualChange);
    }

    @Override
    public void revert() {
        double actualChange = - (percent * duration);
        simulator.getMarket().updateValueByPercent(actualChange);
    }

    @Override
    public String toString() {
        return "Bull Simulation for " + duration + " years at a yearly rate of: " + percent * 100 + "%";
    }

}
