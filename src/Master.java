import gui.*;
import cmd.Handler.CommandHandler;

import javax.swing.*;

/**
 * Starts the application
 */
public class Master {

    public static void main(String args[]){
        CommandHandler commandHandler = new CommandHandler();
        Window mainWindow = new Window(commandHandler);
        LoginView loginView = new LoginView(mainWindow);
        mainWindow.setCurrentView(loginView);
        commandHandler.addObserver(mainWindow);
        mainWindow.setVisible(true);
        mainWindow.pack();
        mainWindow.setTitle("FPTS");

        if (commandHandler.getPortfolioManager().portfolios == null) {
            JOptionPane.showMessageDialog(null,"Error loading Portfolios, please restart application","Oops",JOptionPane.ERROR_MESSAGE);
        }

    }

}
