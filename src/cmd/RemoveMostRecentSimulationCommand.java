package cmd;

import cmd.Handler.CommandHandler;
import model.Simulations.Simulation;

/**
 * Created by ziggypop on 3/11/16.
 *
 */
public class RemoveMostRecentSimulationCommand extends ReversibleCommand {
    private CommandHandler ch;
    private Simulation simulation;

    public RemoveMostRecentSimulationCommand(CommandHandler ch){
        super(ch);
        this.ch = ch;
    }

    @Override
    public void execute() {
        simulation = ch.getSimulator().removeMostRecentSimulation();
    }

    @Override
    public void revertCommand() throws Exception {
        ch.getSimulator().addSimulation(simulation);
    }
}
