package cmd;

import cmd.Handler.CommandHandler;

/**
 * Created by ziggypop on 3/18/16.
 */
public abstract class ReversibleCommand extends Command {

    private CommandHandler ch;
    public ReversibleCommand(CommandHandler ch){
        this.ch = ch; // storing the command handler allows you to push this command to the command stack.
    }

    @Override
    public void execute() throws Exception {
        ch.getCommandHistoryManager().pushToCommandStack(this);
    }

    public abstract void revertCommand() throws Exception;
}
