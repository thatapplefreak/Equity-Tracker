package cmd;

import cmd.Handler.CommandHandler;
import exceptions.CashException;
import model.User.CashAccount;
import model.Transactions.CashTransferTransaction;

/**
 * Created by ziggypop on 3/25/16.
 */
public class TransferCashCommand extends ReversibleCommand{
    private CommandHandler ch;
    private CashAccount originAccount;
    private CashAccount destinationAccount;
    private double amount;

    private CashTransferTransaction cashTransferTransaction;


    public TransferCashCommand(CommandHandler ch,
                               CashAccount originAccount,
                               double amount,
                               CashAccount destinationAccount) {
        super(ch);
        this.ch = ch;
        this.originAccount = originAccount;
        this.amount = amount;
        this.destinationAccount = destinationAccount;
    }


    public void execute() throws CashException{
        try {
            super.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

        cashTransferTransaction = new CashTransferTransaction(originAccount, amount, destinationAccount, CashTransferTransaction.TransactionAction.Transfer);
        ch.getPortfolioManager().getCurrentUserPortfolio().addTransaction(cashTransferTransaction);

        originAccount.sendCashToOtherCashAccount(destinationAccount, amount);


    }

    @Override
    public void revertCommand() throws CashException {
        ch.getPortfolioManager().getCurrentUserPortfolio().removedTransactionFromHistory(cashTransferTransaction);
        destinationAccount.sendCashToOtherCashAccount(originAccount, amount);
    }
}
