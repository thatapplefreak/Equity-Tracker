package cmd;

import cmd.Handler.CommandHandler;
import exceptions.CashException;
import exceptions.SharesException;
import model.User.Holding;
import model.Transactions.HoldingTransaction;
import model.User.UserPortfolio;

/**
 * Created by ziggypop on 3/11/16.
 */
public class SellHoldingCommand extends ReversibleCommand {

    private CommandHandler ch;
    private Holding holding;
    private int number;

    private HoldingTransaction newHoldingTransaction;


    public SellHoldingCommand(CommandHandler ch, Holding holding, int number){
        super(ch);
        this.ch = ch;
        this.holding = holding;
        this.number = number;
    }

    @Override
    public void execute() {
        try {
            super.execute(); // push the command to the command stack.
        } catch (Exception e) {
            e.printStackTrace();
        }

        sellHolding(holding, number);
    }

    /**
     * Sells the a number of shares from a holding after checking if it is ok.
     * This will add money to the CashAccount
     * @param holding The holding that the equities will be sold from.
     * @param number The number of shares to be sold.
     * @return True if the transaction succeeded, False if the transaction falied
     */
    public boolean sellHolding(Holding holding, int number){
        UserPortfolio user = ch.getPortfolioManager().getCurrentUserPortfolio();
        double cost = holding.getTradableEntity().getValue();
        // Check if by selling a holding, you wont exhaust your cash account (ie, an underwater asset)
        try {
            user.getSelectedCashAccount().addCash(cost * number);
        } catch (CashException e){
            System.out.println(e.toString());
            return false;
        }

        //remove the holding shares
        try {
            holding.removeShares(number);
        } catch (SharesException e){
            System.out.println(e.toString());
            //This is a possible point of failure
            try {
                user.getSelectedCashAccount().removeCash(cost);
            } catch (CashException e1) {
                //do nothing
                e1.printStackTrace();
            }
            return false;
        }

        // remove the holding from the portfolio if the holding has 0 shares.
        if (holding.getNumberSharesOwned() == 0) {
            user.getHoldings().remove(holding);
        }

        newHoldingTransaction = new HoldingTransaction(holding,
                number,
                holding.getValue(),
                HoldingTransaction.TransactionAction.SELL);
        //add the transaction to the portfolio
        user.addTransaction(newHoldingTransaction);

        return true;
    }

    /**
     * Precondition: execute has already been sold.
     */
    public void reverseSell() throws SharesException, CashException {
        UserPortfolio user = ch.getPortfolioManager().getCurrentUserPortfolio();

        user.getSelectedCashAccount().addCash(newHoldingTransaction.getCurrentValue() * newHoldingTransaction.getNumber());
        holding.addShares(number);

        user.removedTransactionFromHistory(newHoldingTransaction);

    }

    @Override
    public void revertCommand() throws SharesException, CashException {
        reverseSell();
    }

}
