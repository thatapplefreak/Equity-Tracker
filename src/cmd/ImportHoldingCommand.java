package cmd;

import cmd.Handler.CommandHandler;
import exceptions.SharesException;
import model.MarketStructure.Equity;
import model.MarketStructure.TradableEntity;
import model.Transactions.HoldingTransaction;
import model.User.Holding;
import model.User.UserPortfolio;

/**
 * Created by ziggypop on 3/14/16.
 */
public class ImportHoldingCommand extends ReversibleCommand {

    private HoldingTransaction newHoldingTransaction;
    private Holding activeHolding;

    private CommandHandler ch;
    private TradableEntity tradableEntity;
    private int number;


    public ImportHoldingCommand(CommandHandler ch, TradableEntity tradableEntity, int number){
        super(ch);
        this.ch = ch;
        this.tradableEntity = tradableEntity;
        this.number = number;
    }

    @Override
    public void execute() {
        //ch.getPortfolioManager().getCurrentUserPortfolio().acquireHolding(tradableEntity, number);
        acquireHolding(tradableEntity, number);
    }

    @Override
    public void revertCommand() throws Exception {
        removeHolding();
    }

    /**
     * Adds a holding or adds the number of equities to an existing holding.
     * This will NOT remove money from the cash account.
     * @param tradableEntity Equity to be bought.
     * @param number The number of Equities.
     */
    public void acquireHolding(TradableEntity tradableEntity, int number){
        UserPortfolio user = ch.getPortfolioManager().getCurrentUserPortfolio();

        boolean holdingForEquityExists = false;
        number = Math.abs(number);

        for (Holding holding : user.getHoldings()){
            if (holding.getTradableEntity().equals(tradableEntity)){
                holdingForEquityExists = true;
                try {
                    holding.addShares(number);
                    newHoldingTransaction = new HoldingTransaction(holding,
                            number,
                            tradableEntity.getValue(),
                            HoldingTransaction.TransactionAction.ACQUIRE);

                } catch (SharesException e) {
                    //THIS SHOULD NEVER HAPPEN
                    e.printStackTrace();
                }
                activeHolding = holding;
                break;
            }
        }

        if (!holdingForEquityExists) {
            activeHolding = new Holding(tradableEntity, number);
            // Overwrite the holding transaction if you are just adding
            newHoldingTransaction = new HoldingTransaction(activeHolding,
                    number,
                    activeHolding.getValue(),
                    HoldingTransaction.TransactionAction.ACQUIRE);
            user.addHolding(activeHolding);
        }
        user.addTransaction(newHoldingTransaction);

    }

    public void removeHolding(){
        UserPortfolio user = ch.getPortfolioManager().getCurrentUserPortfolio();
        user.removedTransactionFromHistory(newHoldingTransaction);
        try {
            activeHolding.removeShares(newHoldingTransaction.getNumber());
            if (activeHolding.getNumberSharesOwned() == 0) {
                // This assumes that the holding has already been added to the user.
                user.getHoldings().remove(activeHolding);
            }
        } catch (SharesException e) {
            // external logic should prevent this
            e.printStackTrace();
        }
    }


}
