package cmd;

import cmd.Handler.CommandHandler;
import exceptions.CashException;
import model.Transactions.CashMovementTransaction;
import model.User.CashAccount;
import model.User.UserPortfolio;

/**
 * Created by ziggypop on 3/11/16.
 */
public class AddCashCommand extends ReversibleCommand {

    private CashMovementTransaction ct;

    private CommandHandler ch;
    private double amount;
    private CashAccount cashAccount;

    public AddCashCommand(CommandHandler ch, double cash, CashAccount cashAccount){
        super(ch);
        this.ch = ch;
        this.amount = cash;
        this.cashAccount = cashAccount;
    }


    @Override
    public void execute() throws CashException {
        try {
            super.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        UserPortfolio user = ch.getPortfolioManager().getCurrentUserPortfolio();
        ct = new CashMovementTransaction(amount, cashAccount, CashMovementTransaction.TransactionAction.Deposit);
        cashAccount.addCash(amount);
        user.addTransaction(ct);
    }

    @Override
    public void revertCommand() throws Exception {
        UserPortfolio user = ch.getPortfolioManager().getCurrentUserPortfolio();
        user.removedTransactionFromHistory(ct);
        cashAccount.removeCash(amount);
    }
}
