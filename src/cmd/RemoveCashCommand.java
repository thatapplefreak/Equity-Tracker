package cmd;

import cmd.Handler.CommandHandler;
import exceptions.CashException;
import model.Transactions.CashMovementTransaction;
import model.User.CashAccount;
import model.User.UserPortfolio;

/**
 * Created by ziggypop on 3/11/16.
 */
public class RemoveCashCommand extends ReversibleCommand {

    private CashMovementTransaction ct;

    private CommandHandler ch;
    private double amount;

    public RemoveCashCommand(CommandHandler ch, double cash){
        super(ch);
        this.ch = ch;
        this.amount = cash;
    }


    @Override
    public void execute() throws CashException {
        try {
            super.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        UserPortfolio user = ch.getPortfolioManager().getCurrentUserPortfolio();
        CashAccount cashAccount = user.getSelectedCashAccount();
        cashAccount.removeCash(amount);
        ct = new CashMovementTransaction(amount, cashAccount, CashMovementTransaction.TransactionAction.Withdraw);
        user.addTransaction(ct);
    }

    @Override
    public void revertCommand() throws Exception {
        UserPortfolio user = ch.getPortfolioManager().getCurrentUserPortfolio();
        CashAccount cashAccount = user.getSelectedCashAccount();
        cashAccount.addCash(amount);
        user.removedTransactionFromHistory(ct);
    }


}
