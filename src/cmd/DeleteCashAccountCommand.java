package cmd;

import cmd.Handler.CommandHandler;
import exceptions.CashAccountDeletionException;
import model.User.UserPortfolio;

/**
 * Created by ziggypop on 3/13/16.
 */
public class DeleteCashAccountCommand extends  Command{

    private CommandHandler ch;
    public DeleteCashAccountCommand(CommandHandler ch){
        this.ch = ch;
    }

    @Override
    public void execute() throws CashAccountDeletionException {
        UserPortfolio user = ch.getPortfolioManager().getCurrentUserPortfolio();
        if (user.getCashAccounts().size() <= 1){
            throw new CashAccountDeletionException("You cannot delete your last cash account");
        } else {
            user.getCashAccounts().remove(user.getSelectedCashAccount());
            //Set the selected cash account to the first one in the list.
            user.setSelectedCashAccount(user.getCashAccounts().get(0));
        }

    }
}
