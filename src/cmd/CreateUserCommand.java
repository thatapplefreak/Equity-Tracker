package cmd;

import cmd.Handler.CommandHandler;
import exceptions.CreateUserException;
import model.User.UserPortfolio;

/**
 * Created by ziggypop on 3/7/16.
 *
 *
 */
public class CreateUserCommand extends Command{
    private CommandHandler ch;
    private String username;
    private String plaintextPassword;

    public CreateUserCommand(CommandHandler ch, String username, String plaintextPassword){
        this.ch = ch;
        this.username = username;
        this.plaintextPassword = plaintextPassword;
    }

    @Override
    public void execute() throws CreateUserException {
        UserPortfolio p = new UserPortfolio(username, plaintextPassword);
        // Make sure there is no other with this username
        UserPortfolio search = null;
        for (UserPortfolio portfolio : ch.getPortfolioManager().portfolios) {
            if (portfolio.getUsername().equals(this.username)) {
                search = portfolio;
                break;
            }
        }
        if (search != null) {
            throw new CreateUserException("User Already Exists");
        }
        System.out.println("Created user: " + username);
        ch.getPortfolioManager().portfolios.add(p);
    }
}
