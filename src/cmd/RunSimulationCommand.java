package cmd;

import cmd.Handler.CommandHandler;

/**
 * Created by ziggypop on 3/7/16.
 * A command to run simulations.
 * If true is passed into the constructor, the command will only run one simulation.
 * Otherwise, it will run all of the queued simulations.
 */
public class RunSimulationCommand extends ReversibleCommand {
    private CommandHandler ch;
    private boolean isStep;

    public RunSimulationCommand(CommandHandler ch, boolean isStep){
        super(ch);
        this.ch = ch;
        this.isStep = isStep;
    }

    @Override
    public void execute() {
        ch.getSimulator().runSimulation(isStep);
    }

    @Override
    public void revertCommand() {
        ch.getSimulator().rollBackSimulation(isStep);
    }
}
