package cmd;

import cmd.Handler.CommandHandler;

/**
 * Created by ziggypop on 3/11/16.
 */
public class ResetSimulationCommand extends Command {

    private CommandHandler ch;
    public ResetSimulationCommand(CommandHandler ch){
        this.ch = ch;
    }

    @Override
    public void execute() {
        ch.getSimulator().resetSimulations();
    }
}
