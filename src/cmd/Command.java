package cmd;

/**
 * Created by ziggypop on 3/4/16.
 */
public abstract class Command {

    public abstract void execute() throws Exception;

}
