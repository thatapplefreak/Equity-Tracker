package cmd;

import cmd.Handler.CommandHandler;
import exceptions.CashAccountCreationException;

/**
 * Created by ziggypop on 3/13/16.
 *
 * A Command to create a cash account and add it to the current user.
 */
public class CreateCashAccountCommand extends Command{

    private CommandHandler ch;
    private double amount;
    private String name;

    public CreateCashAccountCommand(CommandHandler ch, double amount, String name){
        this.ch = ch;
        this.amount = amount;
        this.name = name;
    }

    @Override
    public void execute() throws CashAccountCreationException {
        boolean creationSuceeded = ch
                .getPortfolioManager()
                .getCurrentUserPortfolio()
                .createCashAccount(amount, name);
        if (!creationSuceeded){
            throw new CashAccountCreationException("There is already a Cash Account named: "
                    + name
                    + ".\n We were nice and added the initial sum of money to the existing account.");
        }
    }
}
