package cmd;

import cmd.Handler.CommandHandler;
import exceptions.SharesException;
import model.User.Holding;
import model.Transactions.HoldingTransaction;
import model.User.UserPortfolio;

/**
 * Created by ziggypop on 3/14/16.
 */
public class RemoveHoldingCommand extends ReversibleCommand {

    private HoldingTransaction newHoldingTransaction;

    private CommandHandler ch;
    private Holding holding;
    private int number;
    public RemoveHoldingCommand(CommandHandler ch, Holding holding, int number){
        super(ch);
        this.ch = ch;
        this.holding = holding;
        this.number = number;
    }

    @Override
    public void execute() {
        removeHolding(holding, number);
    }

    @Override
    public void revertCommand() throws Exception {
        reAddHolding();
    }

    /**
     * Sells the a number of shares from a holding after checking if it is ok.
     * This will NOT add money to the CashAccount
     * @param holding The holding that the equities will be sold from.
     * @param number The number of shares to be sold.
     */
    public void removeHolding(Holding holding, int number){
        UserPortfolio user = ch.getPortfolioManager().getCurrentUserPortfolio();
        try {
            holding.removeShares(number);
            newHoldingTransaction = new HoldingTransaction(holding,
                    number,
                    holding.getValue(),
                    HoldingTransaction.TransactionAction.REMOVE);
            //add the transaction to the portfolio
            user.addTransaction(newHoldingTransaction);
            if (holding.getNumberSharesOwned() == 0) {
                user.getHoldings().remove(holding);
            }
        } catch (SharesException e){
            System.out.println(e.toString());
        }
    }

    private void reAddHolding(){
        UserPortfolio user = ch.getPortfolioManager().getCurrentUserPortfolio();
        try {
            user.addNumberOfHolding(newHoldingTransaction.getHolding().getTradableEntity(),
                    number);
            user.removedTransactionFromHistory(newHoldingTransaction);
        } catch (SharesException e) {
            // this should not happen given the (non-enforced) conditions required for this method to be called.
            e.printStackTrace();
        }
    }

}
