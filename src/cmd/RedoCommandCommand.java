package cmd;

import cmd.Handler.CommandHandler;

/**
 * Created by ziggypop on 3/30/16.
 */
public class RedoCommandCommand extends Command {
    private CommandHandler ch;
    public RedoCommandCommand(CommandHandler ch){
        this.ch = ch;
    }

    @Override
    public void execute() {
        ch.getCommandHistoryManager().redoMostRecentRevertCommand();
    }
}
