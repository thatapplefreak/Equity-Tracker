package cmd;

import cmd.Handler.CommandHandler;
import model.User.CashAccount;

/**
 * Created by ziggypop on 3/12/16.
 */
public class SelectCashAccountCommand extends Command {

    private CommandHandler ch;
    private CashAccount cashAccount;

    public SelectCashAccountCommand(CommandHandler ch, CashAccount cashAccount){
        this.ch = ch;
        this.cashAccount = cashAccount;
    }

    @Override
    public void execute() throws Exception {
        ch.getPortfolioManager().getCurrentUserPortfolio().setSelectedCashAccount(cashAccount);
    }
}
