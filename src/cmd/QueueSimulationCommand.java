package cmd;

import cmd.Handler.CommandHandler;
import model.Simulations.Simulation;

/**
 * Created by ziggypop on 3/11/16.
 * Adds a simulation to the simulator.
 */
public class QueueSimulationCommand extends ReversibleCommand {

    private CommandHandler ch;
    private Simulation.SimulationEnum simulationEnum;
    private double duration;
    private double perAnumChange;

    public QueueSimulationCommand(CommandHandler ch,
                                  Simulation.SimulationEnum simulationEnum,
                                  double duration,
                                  double perAnumChange){
        super(ch);
        this.ch = ch;
        this.simulationEnum = simulationEnum;
        this.duration = duration;
        this.perAnumChange = perAnumChange;
    }

    @Override
    public void execute() {
        ch.getSimulator().addSimulation(simulationEnum, duration, perAnumChange);
    }

    @Override
    public void revertCommand() throws Exception {
        ch.getSimulator().removeMostRecentSimulation();
    }


}
