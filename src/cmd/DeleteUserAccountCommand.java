package cmd;

import cmd.Handler.CommandHandler;
import model.User.UserPortfolio;

/**
 * Created by ziggypop on 3/13/16.
 */
public class DeleteUserAccountCommand extends Command{

    private CommandHandler ch;
    public DeleteUserAccountCommand(CommandHandler ch){
        this.ch = ch;
    }

    @Override
    public void execute() {
        UserPortfolio current = ch.getPortfolioManager().getCurrentUserPortfolio();
        System.out.println("Deleting user account");
        ch.getPortfolioManager().removeUserPortfolio(current);
        ch.logout(new LogoutCommand(ch)); // logs the user out
    }
}
