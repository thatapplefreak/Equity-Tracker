package cmd;

import cmd.Handler.CommandHandler;

/**
 * Created by ziggypop on 3/30/16.
 */
public class UndoCommandCommand extends Command {

    private CommandHandler ch;
    public UndoCommandCommand(CommandHandler ch){
        this.ch = ch;
    }

    @Override
    public void execute(){
        ch.getCommandHistoryManager().undoLastCommand();
    }
}
