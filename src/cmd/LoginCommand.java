package cmd;

import cmd.Handler.CommandHandler;
import model.User.UserPortfolio;
import exceptions.LoginError;
import model.web.MarketUpdater;

/**
 * Created by ziggypop on 3/4/16.
 */
public class LoginCommand extends Command {
    private CommandHandler ch;
    String username;
    String password;

    public LoginCommand(CommandHandler ch, String username, String password){
        this.ch = ch;
        this.username = username;
        this.password = password;
    }

    @Override
    public void execute() throws LoginError {
        // Get the relevant user
        UserPortfolio portfolio = null;
        for (UserPortfolio p : ch.getPortfolioManager().portfolios) {
            if (p.getUsername().equals(this.username)) {
                portfolio = p;
                break;
            }
        }
        // Throw an error if the user does not exist
        if (portfolio == null) {
            throw new LoginError("User Not Found");
        }
        if (!portfolio.getPassword().verifyPassword(password)) {
            // Wrong Password
            throw new LoginError("Incorrect Password");
        }
        // Correct Username and Password
        System.out.println("User logged in: " + username);
        ch.getPortfolioManager().setCurrentUserPortfolio(portfolio);

        // Set auto update interval
        MarketUpdater.setTimer(portfolio.getMarketUpdateTime());
    }
}
