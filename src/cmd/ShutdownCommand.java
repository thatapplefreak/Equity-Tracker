package cmd;

import cmd.Handler.CommandHandler;
import model.StorageSubsystem.Storage;
import model.web.MarketUpdater;

/**
 * Created by ziggypop on 4/1/16.
 * A command that encapsulates shutting down the system by saving the current state.
 */
public class ShutdownCommand extends Command {

    CommandHandler ch;
    public ShutdownCommand(CommandHandler ch){
        this.ch = ch;
    }

    @Override
    public void execute() {
        Storage.saveUserPortfolios(ch.getPortfolioManager().portfolios);
        System.exit(0);
    }
}
