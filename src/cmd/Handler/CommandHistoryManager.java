package cmd.Handler;

import cmd.Command;
import cmd.ReversibleCommand;

import java.util.LinkedList;

/**
 * Created by ziggypop on 3/30/16.
 *
 * Stores commands that have been executed, allowing undoing and redoing.
 *
 */
public class CommandHistoryManager {
    private LinkedList<ReversibleCommand> commandStack;
    private LinkedList<ReversibleCommand> redoCommandStack;

    public CommandHistoryManager(){
        commandStack = new LinkedList<>();
        redoCommandStack = new LinkedList<>();
    }

    public void pushToCommandStack(ReversibleCommand cmd){
        commandStack.add(cmd);
    }

    public void undoLastCommand(){
        ReversibleCommand lastCommand = commandStack.removeLast();
        try {
            lastCommand.revertCommand();//revert the command
        } catch (Exception e) {
            //THIS SHOULD NEVER HAPPEN
            e.printStackTrace();
        }
        redoCommandStack.add(lastCommand); //add the command to the redo stack
    }

    public void redoMostRecentRevertCommand(){
        ReversibleCommand lastRevert = redoCommandStack.removeLast();
        try {
            lastRevert.execute();
        } catch (Exception e) {
            //THIS SHOULD NEVER HAPPEN
            e.printStackTrace();
        }

    }

    public int getRedoStackSize(){
        return redoCommandStack.size();
    }
    public int getUndoStackSize(){
        return commandStack.size();
    }


}
