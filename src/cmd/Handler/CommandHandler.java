package cmd.Handler;

import cmd.*;
import exceptions.*;
import model.MarketStructure.Market;
import model.Simulations.Simulator;
import model.StorageSubsystem.MarketCSVParser;
import model.User.UserPortfolioManager;
import model.web.MarketUpdater;

import java.util.Observable;

/**
 * Created by ziggypop on 3/18/16.
 * Handles Commands from the GUI
 */
public class CommandHandler extends Observable {

    private static final String EQUITY_CSV_FILE = "data/equities.csv"; // the file where CSVs are stored


    private UserPortfolioManager userPortfolioManager;
    private CommandHistoryManager commandHistoryManager;
    private Market market;
    private Simulator simulator;


    public Exception Error;

    public CommandHandler(){
        //Initialize the Undo/Redo functionality
        commandHistoryManager = new CommandHistoryManager();
        //Initialize the market by parsing the given CSV
        market = MarketCSVParser.createMarket(EQUITY_CSV_FILE);

        // update market from Yahoo finance
        MarketUpdater.setMarket(market);
        MarketUpdater.updateMarket();

        //Initialize the simulator subsystem, giving it a reference to the market.
        simulator = new Simulator(market);
        //Initialize the userPortfolios, passing the market to allow the GSON deserializer
        // to link the holdings belonging to each UserPortfolio to the market.
        userPortfolioManager = new UserPortfolioManager(market);
    }

    public Simulator getSimulator(){
        return this.simulator;
    }
    public Market getMarket(){
        return market;
    }

    public CommandHistoryManager getCommandHistoryManager(){
        return commandHistoryManager;
    }

    public UserPortfolioManager getPortfolioManager(){
        return userPortfolioManager;
    }

    /**
     * Shutdown procedure
     * This should be called when the system is closing
     * It will store the state of the user portfolios to a file for export and restoring the state on the next startup.
     */
    public void shutdown(ShutdownCommand cmd){
        cmd.execute();
    }


    /**
     * Logs the user into the application
     * @param loginCommand The Command to be run
     */
    public void login(LoginCommand loginCommand){
        try {
            loginCommand.execute();
        } catch (LoginError e) {
            Error = e;
            e.printStackTrace();
        }
        updateGUI();
    }

    /**
     * Logs the user out of the application.
     * @param logoutCommand The Command to be run.
     */
    public void logout(LogoutCommand logoutCommand) {
        logoutCommand.execute();
        updateGUI();
    }

    /**
     * Tell the gui something happened
     */
    public void updateGUI() {
        setChanged();
        notifyObservers();
    }

    public void selectCashAccount(SelectCashAccountCommand cmd) {
        try {
            cmd.execute();
        } catch (Exception e) {
            Error = e;
            e.printStackTrace();
        }
        updateGUI();
    }

    public void withdrawCash(RemoveCashCommand cmd) {
        try {
            cmd.execute();
        } catch (CashException e) {
            Error = e;
            e.printStackTrace();
        }
        updateGUI();
    }

    public void deleteCashAccount(DeleteCashAccountCommand cmd){
        try {
            cmd.execute();
        } catch (Exception e) {
            Error = e;
            e.printStackTrace();
        }
        updateGUI();
    }

    public void createUser(CreateUserCommand cmd){
        try {
            cmd.execute();
        } catch (CreateUserException createUserExeption) {
            Error = createUserExeption;
            createUserExeption.printStackTrace();
        }
        updateGUI();
    }

    /**
     * Add a simulation to the queue of simulations to be run.
     */
    public void queueSimulation(QueueSimulationCommand queueSimulationCommand){
        queueSimulationCommand.execute();
        updateGUI();
    }

    /**
     * Reverts all changes the simulator made and clears the simulator.
     * @param resetSimulationCommand The command to be run.
     */
    public void resetSimulations(ResetSimulationCommand resetSimulationCommand){
        resetSimulationCommand.execute();
        updateGUI();
    }

    /**
     * Removes the most recent Simulation from the Simulator.
     * @param removeSimulationCommand The command to be run.
     */
    public void removeSimulation(RemoveMostRecentSimulationCommand removeSimulationCommand){
        removeSimulationCommand.execute();
        updateGUI();
    }

    /**
     * Run the simulation, effecting the value of the market.
     */
    public void runSimulation(RunSimulationCommand runCommand){
        runCommand.execute();
        updateGUI();
    }

    /**
     * Buys an equity (Equity), adding it to the current user portfolio's holdings.
     * This will remove money from the selectedCashAccount of the user.
     * @param buyEquityCommand The Command to be run.
     */
    public void buyEquity(BuyEquityCommand buyEquityCommand){
        try {
            buyEquityCommand.execute();
        } catch (Exception e) {
            Error = e;
            e.printStackTrace();
        }
        updateGUI();
    }

    /**
     * Sells an equity (Equity), removing it from the user portfolio's holdings.
     * This will add money to the selectedCashAccount of the user.
     * @param sellEquityCommand The Command to be run.
     */
    public void sellEquity(SellHoldingCommand sellEquityCommand){
        try {
            sellEquityCommand.execute();
        } catch (Exception e) {
            Error = e;
            e.printStackTrace();
        }
        updateGUI();
    }

    /**
     * Adds cash to the currentUser's selectedCashAccount.
     * @param addCashCommand The Command to be run.
     */
    public void addCashToAccount(AddCashCommand addCashCommand){
        try {
            addCashCommand.execute();
        } catch (Exception e) {
            e.printStackTrace();
            Error = e;
        }
        updateGUI();
    }

    /**
     * Removes cash from the currentUser's selectedCashAccount.
     * @param removeCashCommand The Command to be run.
     */
    public void removeCashFromAccount(RemoveCashCommand removeCashCommand){
        try {
            removeCashCommand.execute();
        } catch (Exception e) {
            e.printStackTrace();
            Error=e;
        }
        updateGUI();
    }

    /**
     * Creates a CashAccount for the currentUser.
     * @param createCashAccountCommand The Command to be run.
     */
    public void createCashAccount(CreateCashAccountCommand createCashAccountCommand){
        try {
            createCashAccountCommand.execute();
        } catch (CashAccountCreationException e) {
            e.printStackTrace();
            Error = e;
        }
        updateGUI();
    }

    public void deleteUserAccount(DeleteUserAccountCommand cmd){
        cmd.execute();
    }

    /**
     * Undoes the most recent reversible command.
     * @param cmd The command that undoes the most recent command.
     */
    public void undoCommand(UndoCommandCommand cmd){
        cmd.execute();
        updateGUI();
    }

    /**
     * redoes command that has been undone.
     * @param cmd The command that undoes the most recently undone command.
     */
    public void redoCommand(RedoCommandCommand cmd){
        cmd.execute();
        updateGUI();
    }

    /**
     * Adds a holding to a user portfolio without having to pay any money.
     * @param cmd The command to be run.
     */
    public void importHolding(ImportHoldingCommand cmd){
        cmd.execute();
    }

    /**
     * Removes a holding from a user portfolio without receiving any money.
     * @param cmd The command to be run.
     */
    public void removeHolding(RemoveHoldingCommand cmd){
        cmd.execute();
    }

    /**
     * Moves cash from one account to another account.
     * @param cmd The command to be run.
     */
    public void transferCashToOtherAccount(TransferCashCommand cmd){
        try {
            cmd.execute();
        } catch (CashException e) {
            e.printStackTrace();
            Error = e;
        }
        updateGUI();
    }


}
