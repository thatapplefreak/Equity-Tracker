package cmd;

import cmd.Handler.CommandHandler;
import exceptions.CashException;
import exceptions.SharesException;
import model.MarketStructure.Equity;
import model.MarketStructure.TradableEntity;
import model.Transactions.HoldingTransaction;
import model.User.Holding;
import model.User.UserPortfolio;

/**
 * Created by ziggypop on 3/11/16.
 * Command to buy equity.
 */
public class BuyEquityCommand extends ReversibleCommand {
    private CommandHandler ch;
    private TradableEntity tradableEntity;
    private int number;

    private HoldingTransaction newHoldingTransaction;
    private Holding newHolding;


    public BuyEquityCommand(CommandHandler ch, TradableEntity tradableEntity, int number){
        super(ch);

        this.ch = ch;
        this.tradableEntity = tradableEntity;
        this.number = number;
    }

    @Override
    public void execute() throws CashException, SharesException {
        try {
            super.execute(); // push the command to the command stack.
        } catch (Exception e) {
            e.printStackTrace();
        }

        UserPortfolio user = ch.getPortfolioManager().getCurrentUserPortfolio();

        boolean holdingForEquityExists = false;
        if (number <= 0){
            throw new SharesException("You must have a positive number of shares.");
        }


        for (Holding holding : user.getHoldings()){
            if (holding.getTradableEntity().equals(tradableEntity)){
                holdingForEquityExists = true;
                try {
                    holding.addShares(number);
                } catch (SharesException e) {
                    //THIS SHOULD NEVER HAPPEN
                    e.printStackTrace();
                }
                break;
            }
        }
        if (!holdingForEquityExists){ // If the holding for this equity does not already exist...

            newHolding = new Holding(tradableEntity, number);
            if (tradableEntity == null){
                System.out.print("auntaoehuntaoheuntaoheunt");
            }
            double cost = newHolding.getValue();
            //try to deduct money from the cash account
            try {
                user.getSelectedCashAccount().removeCash(cost);
                newHoldingTransaction = new HoldingTransaction(newHolding,
                        number,
                        newHolding.getValue(),
                        HoldingTransaction.TransactionAction.BUY);
                //add the transaction and holding to the portfolio
                user.addTransaction(newHoldingTransaction);
                user.addHolding(newHolding);
            } catch (CashException e) {
                throw e;
            }

        }
    }

    @Override
    public void revertCommand() throws Exception {
        UserPortfolio user = ch.getPortfolioManager().getCurrentUserPortfolio();

        user.removedTransactionFromHistory(newHoldingTransaction);


        double cost = newHolding.getTradableEntity().getValue();
        // Check if by selling a holding, you wont exhaust your cash account (ie, an underwater asset)
        try {
            user.getSelectedCashAccount().addCash(cost);
        } catch (CashException e){
            System.out.println(e.toString());
            //return false;
        }

        //remove the holding shares
        try {
            newHolding.removeShares(number);
        } catch (SharesException e){
            System.out.println(e.toString());
            //This is a possible point of failure
            try {
                user.getSelectedCashAccount().removeCash(cost);
            } catch (CashException e1) {
                //do nothing
                e1.printStackTrace();
            }
            //return false;
        }

        // remove the holding from the portfolio if the holding has 0 shares.
        if (newHolding.getNumberSharesOwned() == 0) {
            user.deleteHolding(newHolding);
        }
    }

}
