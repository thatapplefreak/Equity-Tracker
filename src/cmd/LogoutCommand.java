package cmd;

import cmd.Handler.CommandHandler;
import model.web.MarketUpdater;

/**
 * Created by ziggypop on 3/11/16.
 */
public class LogoutCommand extends Command{
    private CommandHandler ch;
    public LogoutCommand(CommandHandler ch){
        this.ch = ch;
    }

    @Override
    public void execute() {
        ch.getPortfolioManager().unsetCurrentUserPortfolio();
        MarketUpdater.cancelTimer();
    }
}
