package exceptions;

/**
 * Created by byronzaharako on 3/10/16.
 */
public class LoginError extends Exception {

    public LoginError(String msg) {
        super(msg);
    }

}
