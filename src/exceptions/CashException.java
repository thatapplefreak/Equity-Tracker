package exceptions;

/**
 * Created by ziggypop on 3/13/16.
 */
public class CashException extends Exception {
       public CashException(){}

       public CashException(String message){
           super(message);
       }
   }