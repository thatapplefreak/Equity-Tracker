package exceptions;

/**
 * Created by ziggypop on 3/13/16.
 */
public class CashAccountDeletionException extends Exception {
    public CashAccountDeletionException(String msg){
        super(msg);
    }
}
