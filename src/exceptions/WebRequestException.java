package exceptions;

/**
 * Created by ryan on 3/26/16.
 */
public class WebRequestException extends Exception {
    public WebRequestException(String msg) { super(msg); }
}
