package exceptions;

/**
 * Created by ziggypop on 3/11/16.
 */
public class HoldingNotFoundException extends Exception {

    public HoldingNotFoundException(String msg) {
        super(msg);
    }
}
