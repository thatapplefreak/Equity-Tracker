package exceptions;

/**
 * Created by byronzaharako on 3/30/16.
 */
public class WatchListException extends Exception {
    public WatchListException(String msg) {super(msg);}
}
