package exceptions;

/**
 * Created by byronzaharako on 3/10/16.
 */
public class CreateUserException extends Exception {
    public CreateUserException(String msg) {
        super(msg);
    }
}
