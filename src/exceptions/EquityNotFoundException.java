package exceptions;

/**
 * Created by ziggypop on 3/13/16.
 */
public class EquityNotFoundException extends Exception {
        public EquityNotFoundException() {}
        public EquityNotFoundException(String message){
            super(message);
        }
    }