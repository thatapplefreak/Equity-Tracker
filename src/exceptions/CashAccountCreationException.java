package exceptions;

/**
 * Created by ziggypop on 3/13/16.
 */
public class CashAccountCreationException extends Exception {
    public CashAccountCreationException(String msg){
        super(msg);
    }
}
